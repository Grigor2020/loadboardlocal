'use strict';
const servers = require('./server.json');

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('servers', servers, {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('servers', null, {});
  }
};