'use strict';
const cables = require('./cable.json');

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('cables', cables, {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('cables', null, {});
  }
};