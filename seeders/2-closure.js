'use strict';
const closures = require('./closure.json');

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('closures', closures, {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('closures', null, {});
  }
};