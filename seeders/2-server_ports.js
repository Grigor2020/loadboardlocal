'use strict';
const serverPorts = require('./server_port.json');

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('server_ports', serverPorts, {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('server_ports', null, {});
  }
};