'use strict';
const spliceLibs = require('./splice_lib.json');

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('splice_libs', spliceLibs, {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('splice_libs', null, {});
  }
};