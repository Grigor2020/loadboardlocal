'use strict';
const nodes = require('./node.json');

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('nodes', nodes, {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('nodes', null, {});
  }
};