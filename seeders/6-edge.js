'use strict';
const edges = require('./edge.json');

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('edges', edges, {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('edges', null, {});
  }
};