'use strict';
const e_points = require('./e_point.json');

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('e_points', e_points, {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('e_points', null, {});
  }
};