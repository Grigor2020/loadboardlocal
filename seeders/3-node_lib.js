'use strict';
const nodeLib = require('./node_lib.json');

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('node_libs', nodeLib, {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('node_libs', null, {});
  }
};