'use strict';
const brands = require('./brand.json');

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('brands', brands, {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('brands', null, {});
  }
};