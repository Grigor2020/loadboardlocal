'use strict';
const fibers = require('./fiber.json');

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('fibers', fibers, {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('fibers', null, {});
  }
};