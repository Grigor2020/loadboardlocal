'use strict';
const splices = require('./splice.json');

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('splices', splices, {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('splices', null, {});
  }
};