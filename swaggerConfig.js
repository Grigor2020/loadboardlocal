const swaggerJSDoc = require('swagger-jsdoc');

const swaggerHost = process.env.SWAGGER_HOST || 'http://localhost:3000';

const swaggerDefinition = {
  info: {
    title: 'Caiman NMS API Definition.',
    version: '1.0.0',
    description: 'Caiman NMS Backend API',
    contact: {
      email: 'tigran.vardanyan@caiman.am',
    },
  },
  host: swaggerHost.replace('http://', '').replace('https://', ''),
  basePath: '/',
  schemes: 'http',
  consumes: ['application/x-www-form-urlencoded', 'application/json'],
  produces: 'application/json',
};

const options = {
  swaggerDefinition,
  apis: ['./swagger/**/*.yaml'],
};

module.exports = function SwaggerConfig() {
  const swaggerDocument = swaggerJSDoc(options);
  return swaggerDocument;
};
