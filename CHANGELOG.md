# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### 0.1.1 (2021-06-21)


### Features

* docker config ([17749b8](https://bitbucket.org/pfns/backend/commit/17749b827a6d6f3e38d7b773aea11db4063bf7c8))


### Bug Fixes

* bable build fix ([e990c57](https://bitbucket.org/pfns/backend/commit/e990c57c585ce033957fce247446d1e229c5f744))
* cli validator ([bd9e0c9](https://bitbucket.org/pfns/backend/commit/bd9e0c907ab753c9df11fab26cdb5febc0759605))
* create database ([375e29d](https://bitbucket.org/pfns/backend/commit/375e29dd5ab0a552038a8e6cf5207e467bd36357))
* docker pipeline fix ([e47033b](https://bitbucket.org/pfns/backend/commit/e47033b2fac4a3dc8d2ed6fb3a3d93efe87c8e0a))
* docker-compose path ([f82ae92](https://bitbucket.org/pfns/backend/commit/f82ae925e4794126999b3292bcc18805923f3bab))
* docker-compose path ([59bfd63](https://bitbucket.org/pfns/backend/commit/59bfd63c8ce05f238968425007966d3efce703e8))
* path issue ([63ca3ab](https://bitbucket.org/pfns/backend/commit/63ca3abc09438cf7c0d201d8690ff28dff78b3bb))
* path issue fix ([1f39d60](https://bitbucket.org/pfns/backend/commit/1f39d60a2cfdf5d122f2c24be665d2fe9a75ff84))
* pipeline dokerhub name ([0394e5e](https://bitbucket.org/pfns/backend/commit/0394e5e025abc85708fe2c33890d2d1fe6e2301d))
* require swagger jsdoc ([17c6bfe](https://bitbucket.org/pfns/backend/commit/17c6bfe8960c02c1c18949e121dd87e8d0273fa6))
* seeders ([9db4fcf](https://bitbucket.org/pfns/backend/commit/9db4fcf09d5af613278a7e422695c596f2a26f73))
* template files added ([40b66c2](https://bitbucket.org/pfns/backend/commit/40b66c20ca14e6967efeafb41d58db364daf8e23))
