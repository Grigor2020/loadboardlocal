FROM node:14.15.3

WORKDIR /usr/src/app
COPY . .
RUN npm install
RUN npm run build
RUN rm -rf node_modules && rm -rf src
ENV NODE_ENV production
RUN npm install --only=production --silent --no-audit
RUN ls
CMD npm run start-prod