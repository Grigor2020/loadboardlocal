/* eslint-disable no-console */
import http from 'http';
import app from './app';

process.env.NODE_ENV = (process.env.NODE_ENV || 'development');

const server = http.createServer(app());
server.listen(process.env.PORT, process.env.HOST, () => {
  console.log('api listening at http://%s:%s', server.address().address, server.address().port, ' pid: ', process.pid);
  console.log(new Date().toLocaleString());
});
