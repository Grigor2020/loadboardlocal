import { execFile } from 'child_process';

const dataFromDb = [
  {
    id: 1, name: 'main', active: true, schedule: 1,
  },
  {
    id: 2, name: 'template', active: true, schedule: 1,
  },
];

const asyncIterable = {
  [Symbol.asyncIterator]() {
    return {
      i: 0,
      data: dataFromDb?.filter((job) => job.active),
      next() {
        if (this.i < this.data.length) {
          return Promise.resolve({ value: this.data[this.i++], done: false });
        }
        return Promise.resolve({ done: true });
      },
    };
  },
};

export default function mainJob(timer) {
  (async function () {
    for await (const job of asyncIterable) {
      console.log(`JOB_ID:${job.id}`);
      if (!(timer % job.schedule)) {
        execFile('node', [`./src/jobs/${job.name}.js`], (error, stdout, stderr) => {
          if (error) {
            throw error;
          }
          console.log(stdout);
        });
      }
    }
  }());
}