const fs = require('fs');
const path = require('path');

const templatePath = path.join(__dirname, '..', 'template');
const modulePath = path.join(__dirname, '..', '..', 'app', 'api');
const swaggerPath = path.join(__dirname, '..', '..', '..', 'swagger');
const modelPath = path.join(__dirname, '..', '..', 'app', 'models');
const moduleIndexPath = path.join(__dirname, '..', '..', 'app', 'index.js');

module.exports = {
  makeModel: (data) => {
    const modelName = data.modelName && data.modelName[0] ? data.modelName : data.moduleName;
    const tableName = data.tableName && data.tableName[0] ? data.tableName : `${data.moduleName}s`;
    const { moduleName, moduleDirectory } = data;

    fs.mkdirSync(path.join(modulePath, moduleDirectory, moduleName));
    ['service', 'router', 'validator', 'controller', 'repository'].forEach((source) => {
      const file = path.join(templatePath, `${source}.js`);
      let content = fs.readFileSync(file, 'utf-8');
      content = content.replace('%%source%%', moduleName);
      fs.writeFileSync(path.join(modulePath, moduleDirectory, moduleName, `${source}.js`), content, 'utf-8');
    });

    const swaggerFile = path.join(templatePath, 'swagger.yaml');
    let swaggerContent = fs.readFileSync(swaggerFile, 'utf-8');
    swaggerContent = swaggerContent.split('%%source%%').join(moduleName);
    swaggerContent = swaggerContent.split('%%route%%').join(path.join(moduleDirectory, moduleName));
    swaggerContent = swaggerContent.split('%%Source%%').join(moduleName.charAt(0).toUpperCase() + moduleName.slice(1));
    fs.writeFileSync(path.join(swaggerPath, `${moduleName}.yaml`), swaggerContent, 'utf-8');

    if (data.model) {
      const modelFile = path.join(templatePath, 'model.js');
      let modelContent = fs.readFileSync(modelFile, 'utf-8');
      modelContent = modelContent.split('%%modelName%%').join(modelName);
      modelContent = modelContent.split('%%tableName%%').join(tableName);
      fs.writeFileSync(path.join(modelPath, `${moduleName}.js`), modelContent, 'utf-8'); 
    }
  },
};
