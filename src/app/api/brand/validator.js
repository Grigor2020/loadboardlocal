import { checkSchema } from 'express-validator/check';

import models from '../../models';
import { coreValidator, errorMessage } from '../../core/validator';

const validator = Object.create(coreValidator);

export default Object.assign(validator, {
  store: checkSchema({
    name: coreValidator.name,
    shortName: { ...coreValidator.name, optional: true },
    address: { ...coreValidator.address, optional: true },
    phone: { ...coreValidator.phone, optional: true },
    email: { ...coreValidator.email, optional: true },
    description: { ...coreValidator.description, optional: true },
    countryId: coreValidator.countryInBody,
    logoId: { ...coreValidator.fileInBody, optional: true },
  }),

  upsert: checkSchema({
    id: {
      in: ['params'],
      errorMessage: (val) => `The resource with ID: ${val} is deleted`,
      custom: {
        errorMessage: errorMessage.idDeleted,
        async options(id) {
          const response = await models.brand.findOne({
            where: { id, deletedAt: { [models.Op.ne]: null } },
            paranoid: false,
          });
          return !response;
        },
      },
    },
    name: coreValidator.name,
    shortName: { ...coreValidator.name, optional: true },
    address: { ...coreValidator.address, optional: true },
    phone: { ...coreValidator.phone, optional: true },
    email: { ...coreValidator.email, optional: true },
    description: { ...coreValidator.description, optional: true },
    countryId: coreValidator.countryInBody,
    logoId: { ...coreValidator.fileInBody, optional: true },
  }),

  update: checkSchema({
    id: coreValidator.brandId,
    name: { ...coreValidator.name, optional: true },
    shortName: { ...coreValidator.name, optional: { options: { nullable: true } } },
    address: { ...coreValidator.address, optional: { options: { nullable: true } } },
    phone: { ...coreValidator.phone, optional: { options: { nullable: true } } },
    email: { ...coreValidator.email, optional: true },
    description: { ...coreValidator.description, optional: { options: { nullable: true } } },
    countryId: { ...coreValidator.countryInBody, optional: true },
    logoId: { ...coreValidator.fileInBody, optional: true },
  }),

  isExist: checkSchema({
    id: coreValidator.brandId,
  }),

  isDeleted: checkSchema({
    id: {
      in: ['params'],
      errorMessage: (val) => `The resource with ID: ${val} is not deleted`,
      custom: {
        errorMessage: errorMessage.idNotDeleted,
        async options(id) {
          const response = await models.brand.findOne({
            where: { id, deletedAt: { [models.Op.ne]: null } },
            paranoid: false,
          });
          return !!response;
        },
      },
    },
  }),

  getAll: checkSchema({
    offset: { ...coreValidator.offset, optional: true },
    limit: { ...coreValidator.limit, optional: true },
    name: { ...coreValidator.nameInQuery, optional: true },
    include: {
      ...coreValidator.queryString,
      optional: true,
      custom: {
        errorMessage: 'The include and exclude fields cannot exist the same time',
        options: (include, { req }) => {
          if (req.query.exclude) return false;
          return true;
        },
      },
    },
    exclude: {
      ...coreValidator.queryString,
      optional: true,
      custom: {
        errorMessage: 'The include and exclude fields cannot exist the same time',
        options: (exclude, { req }) => {
          if (req.query.include) return false;
          return true;
        },
      },
    },
  }),
});
