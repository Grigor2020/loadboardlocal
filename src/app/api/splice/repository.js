import coreRepository from '../../core/repository';

const repository = Object.create(coreRepository);

export default Object.assign(repository, {
  modelName: 'splice',
});
