import gjv from 'geojson-validation';
import { checkSchema } from 'express-validator/check';

import models from '../../models';

import { coreValidator, errorMessage } from '../../core/validator';

const validator = Object.create(coreValidator);

export default Object.assign(validator, {
  modelName: 'node',
  store: checkSchema({
    geom: {
      in: ['body'],
      errorMessage: errorMessage.geom,
      isEmpty: { negated: true },
      custom: {
        errorMessage: errorMessage.jsonError,
        options: (json) => gjv.isPoint(json),
      },
    },
    nodeLibId: coreValidator.nodeLibInBody,
  }),

  upsert: checkSchema({
    id: {
      in: ['params'],
      errorMessage: (val) => `The resource with ID: ${val} is deleted`,
      custom: {
        errorMessage: errorMessage.idDeleted,
        async options(id) {
          const response = await models.node.findByPk(id, { paranoid: false });
          return !!response;
        },
      },
    },
    geom: {
      in: ['body'],
      errorMessage: errorMessage.geom,
      isEmpty: { negated: true },
      custom: {
        errorMessage: errorMessage.jsonError,
        options: (json) => gjv.isPoint(json),
      },
    },
    nodeLibId: coreValidator.nodeLibInBody,
  }),

  update: checkSchema({
    id: coreValidator.nodeId,
    geom: {
      in: ['body'],
      errorMessage: errorMessage.geom,
      optional: true,
      custom: {
        errorMessage: errorMessage.jsonError,
        options: (json) => gjv.isPoint(json),
      },
    },
    nodeLibId: { ...coreValidator.nodeLibInBody, optional: true },
  }),

  isExist: checkSchema({
    id: coreValidator.nodeId,
  }),

  getAll: checkSchema({
  }),
});
