import coreService from '../../core/service';
import repository from './repository';
import { ServiceError } from '../../utils/error_handler';

const service = Object.create(coreService);

export default Object.assign(service, {
  async store(data) {
    try {
      const node = await repository.create({
        ...data,
      });
      return node;
    } catch (error) {
      throw new ServiceError(error);
    }
  },
  getByPk(pk) {
    try {
      return repository.findByPk(pk);
    } catch (error) {
      throw new ServiceError(error);
    }
  },
  upsert(data, id) {
    return repository.upsert({
      ...data,
      id,
    });
  },
  update(data, id) {
    try {
      return repository.update(
        data,
        { id },
      );
    } catch (error) {
      throw new ServiceError();
    }
  },
  getAll() {
    return repository.findAll({});
  },
  remove(id) {
    try {
      return repository.softDelete({ id });
    } catch (error) {
      throw new ServiceError();
    }
  },
});
