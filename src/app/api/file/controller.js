import service from './service';
import cw from '../../core/controller';

export const getAll = cw(() => service.getAll());

export const get = cw((req) => service.getByPk(req.params.id));

export const storeMedias = cw((req) => service.storeMedias(req.body, req.extensions, req.files));

export const store = cw((req) => service.store(req.body, req.fileExtension, req.file));

export const upsert = cw((req) => service.upsert(req.body, req.params.id));

export const update = cw((req) => service.update(req.body, req.params.id));

export const remove = cw((req) => service.remove(req.params.id));

export const storeFolder = cw((req) => service.storeFolder(req.body));

export const updateFolder = cw((req) => service.updateFolder(req.body, req.params.id));

export const removeFolder = cw((req) => service.removeFolder(req.params.id));
