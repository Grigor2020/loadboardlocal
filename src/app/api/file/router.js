import * as controller from './controller';
import validator from './validator';
import { fileUpload, multipleMediaUpload } from '../../utils/file_upload';

export default (router) => {
  // ----------------------------------------------------------------------------------------------
  router.get('/', validator.getAll, controller.getAll);
  // ----------------------------------------------------------------------------------------------
  router.get('/:id', validator.isExist, controller.get);
  // ----------------------------------------------------------------------------------------------
  router.post('/media', validator.store, multipleMediaUpload, controller.storeMedias);
  // ----------------------------------------------------------------------------------------------
  router.post('/', validator.store, fileUpload, controller.store);
  // ----------------------------------------------------------------------------------------------
  router.put('/:id', validator.upsert, controller.upsert);
  // ----------------------------------------------------------------------------------------------
  router.patch('/:id', validator.update, controller.update);
  // ----------------------------------------------------------------------------------------------
  router.delete('/:id', validator.isExist, controller.remove);
  // ----------------------------------------------------------------------------------------------
  router.post('/folder', validator.storeFolder, controller.storeFolder);
  // ----------------------------------------------------------------------------------------------
  router.patch('/folder/:id', validator.updateFolder, controller.updateFolder);
  // ----------------------------------------------------------------------------------------------
  router.delete('/folder/:id', validator.isExist, controller.removeFolder);
  // ----------------------------------------------------------------------------------------------
};
