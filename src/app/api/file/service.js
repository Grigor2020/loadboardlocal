import uuid from 'uuid/v4';
import path from 'path';
import { promises as fs } from 'fs';
import coreService from '../../core/service';
import repository from './repository';
import { ServiceError } from '../../utils/error_handler';

const service = Object.create(coreService);

export default Object.assign(service, {
  /**
   *  Get All available resources
   *  @return {Array} array of resource ORM objects
   */
  getAll() {
    try {
      return repository.findAll({});
    } catch (error) {
      throw new ServiceError(error);
    }
  },

  /**
   *  Get One available resources
   *  @param {Number} pk : resource ID
   *  @return {Object} resource ORM object
   */
  getByPk(pk) {
    try {
      return repository.findByPk(pk);
    } catch (error) {
      throw new ServiceError(error);
    }
  },

  /**
   *  Create resources
   *  @param {Object} data : resource data object
   *  @return {Object} resource ORM object
   */
  async storeMedias(data, extensions, files) {
    try {
      const metadatas = [];
      const insertData = {};
      let parentPath = '';
      if (data.parentId) {
        const folder = await repository.findOne({ id: data.parentId });
        parentPath = path.join(folder.path, folder.id);
        insertData.parentId = data.parentId;
      }
      // eslint-disable-next-line no-restricted-syntax
      for await (const [index, file] of files.entries()) {
        const { format, width, height } = extensions[index];
        insertData.id = uuid();

        const filePath = path.join(global.uploadFolder, parentPath, `${insertData.id}.${format}`);
        if (width && height) insertData.dimensions = { width, height };

        insertData.size = file.size / 1000; // file size in kb
        insertData.extension = format;
        insertData.name = file.name;
        insertData.path = parentPath;

        await file.mv(filePath);

        const created = await repository.create({
          ...insertData,
        });
        created.path = path.join(created.path, `${created.id}.${created.extension}`);
        metadatas.push(created);
      }
      return metadatas;
    } catch (error) {
      throw new ServiceError(error);
    }
  },

  /**
   *  Create resources
   *  @param {Object} data : resource data object
   *  @return {Object} resource ORM object
   */
  async store(data, extensions, file) {
    try {
      const { format, width, height } = extensions;
      const insertData = {
        id: uuid(),
      };
      if (width && height) insertData.dimensions = { width, height };

      let parentPath = '';
      if (data.parentId) {
        const folder = await repository.findOne({ id: data.parentId });
        parentPath = path.join(folder.path, folder.id);
        insertData.parentId = folder.id;
      }
      const filePath = path.join(global.uploadFolder, parentPath, `${insertData.id}.${format}`);
      insertData.size = file.size / 1000; // file size in kb
      insertData.extension = format;
      insertData.name = file.name;
      insertData.path = parentPath;

      await file.mv(filePath);

      const created = await repository.create({
        ...insertData,
      });
      created.path = path.join(created.path, `${created.id}.${created.extension}`);
      return created;
    } catch (error) {
      throw new ServiceError(error);
    }
  },

  /**
   *  Update or Create resources
   *  @param {Object} data : resource data object
   *  @param {Object} id : resource ID
   *  @return {Object} resource ORM object
   */
  upsert(data, id) {
    try {
      return repository.upsert({
        ...data,
        id,
      });
    } catch (error) {
      throw new ServiceError(error);
    }
  },

  /**
   *  Update resources
   *  @param {Object} data : resource data object
   *  @param {Object} id : resource ID
   *  @return {Object} resource ORM object
   */
  update(data, id) {
    try {
      return repository.update(
        data,
        { id },
      );
    } catch (error) {
      throw new ServiceError(error);
    }
  },

  /**
   *  Remove resources
   *  @param {Object} id : resource ID
   *  @return {Object} resource ORM object
   */
  remove(id) {
    try {
      return repository.softDelete({ id });
    } catch (error) {
      throw new ServiceError();
    }
  },

  async storeFolder(data) {
    try {
      const insertData = {
        id: uuid(),
      };

      let parentPath = '';
      if (data.parentId) {
        const folder = await repository.findOne({ id: data.parentId });
        parentPath = path.join(folder.path, folder.id);
        insertData.parentId = folder.id;
      }
      const folderPath = path.join(global.uploadFolder, parentPath, insertData.id);
      insertData.name = data.name;
      insertData.path = parentPath;
      insertData.type = 'FOLDER';

      await fs.mkdir(folderPath);
      const created = await repository.create({
        ...insertData,
      });
      created.path = path.join(created.path, created.id);
      return created;
    } catch (error) {
      throw new ServiceError(error);
    }
  },

  /**
   *  Update resources
   *  @param {Object} data : resource data object
   *  @param {Object} id : resource ID
   *  @return {Object} resource ORM object
   */
  updateFolder(data, id) {
    try {
      return repository.update(
        data,
        { id },
      );
    } catch (error) {
      throw new ServiceError(error);
    }
  },

  /**
   *  Remove resources
   *  @param {Object} id : resource ID
   *  @return {Object} resource ORM object
   */
  removeFolder(id) {
    try {
      return repository.softDelete({ id });
    } catch (error) {
      throw new ServiceError();
    }
  },

});
