import { checkSchema } from 'express-validator/check';

import models from '../../models';
import { coreValidator, errorMessage } from '../../core/validator';

const validator = Object.create(coreValidator);

export default Object.assign(validator, {
  store: checkSchema({
    parentId: { ...coreValidator.folderInBody, optional: true },
  }),

  upsert: checkSchema({
    id: {
      in: ['params'],
      errorMessage: (val) => `The resource with ID: ${val} is deleted`,
      custom: {
        errorMessage: errorMessage.idDeleted,
        async options(id) {
          const response = await models.file.findByPk(id, { paranoid: false });
          return !!response;
        },
      },
    },
    parentId: { ...coreValidator.folderInBody, optional: true },
  }),

  update: checkSchema({
    id: coreValidator.fileId,
    parentId: { ...coreValidator.folderInBody, optional: true },
  }),

  isExist: checkSchema({
    id: coreValidator.fileId,
  }),

  getAll: checkSchema({
  }),

  storeFolder: checkSchema({
    name: coreValidator.name,
    parentId: { ...coreValidator.folderInBody, optional: true },
  }),

  updateFolder: checkSchema({
    id: coreValidator.fileId,
    parentId: { ...coreValidator.folderInBody, optional: true },
  }),
});
