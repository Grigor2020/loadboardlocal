import { checkSchema } from 'express-validator/check';

import models from '../../models';
import { coreValidator, errorMessage, isResourceExist } from '../../core/validator';

const validator = Object.create(coreValidator);

const validationFields = {
  id: {
    in: ['params'],
    errorMessage: (val) => `The department with ID: ${val} does not exist`,
    custom: { options: (id) => isResourceExist(id, models.department) },
  },
  idInBody: {
    in: ['body'],
    optional: true,
    errorMessage: (val) => `The department with ID: ${val} does not exist`,
    custom: { options: (id) => (id || id === 0 ? isResourceExist(id, models.department) : true) },
  },
  userIdInBody: {
    in: ['body'],
    optional: true,
    errorMessage: (val) => `The user with ID: ${val} does not exist`,
    custom: { options: (id) => (id || id === 0 ? isResourceExist(id, models.user) : true) },
  },
};

export default Object.assign(validator, {
  store: checkSchema({
    name: coreValidator.name,
    shortName: { ...coreValidator.name, optional: true },
    address: { ...coreValidator.address, optional: true },
    phone: { ...coreValidator.phone, optional: true },
    email: { ...coreValidator.email, optional: true },
    description: { ...coreValidator.description, optional: true },
    parentId: validationFields.idInBody,
    headId: validationFields.userIdInBody,
  }),

  upsert: checkSchema({
    id: {
      in: ['params'],
      errorMessage: (val) => `The department with ID: ${val} is deleted`,
      custom: {
        errorMessage: errorMessage.idDeleted,
        async options(id) {
          const response = await models.department.findOne({
            where: { id, deletedAt: { [models.Op.ne]: null } },
            paranoid: false,
          });
          return !response;
        },
      },
    },
    name: coreValidator.name,
    shortName: { ...coreValidator.name, optional: true },
    address: { ...coreValidator.address, optional: true },
    phone: { ...coreValidator.phone, optional: true },
    email: { ...coreValidator.email, optional: true },
    description: { ...coreValidator.description, optional: true },
    parentId: validationFields.idInBody,
    headId: validationFields.userIdInBody,
  }),

  update: checkSchema({
    id: validationFields.id,
    name: { ...coreValidator.name, optional: true },
    shortName: { ...coreValidator.name, optional: true },
    address: { ...coreValidator.address, optional: true },
    phone: { ...coreValidator.phone, optional: true },
    email: { ...coreValidator.email, optional: true },
    description: { ...coreValidator.description, optional: true },
    parentId: validationFields.idInBody,
    headId: validationFields.userIdInBody,
  }),

  isExist: checkSchema({
    id: validationFields.id,
  }),

  isDeleted: checkSchema({
    id: {
      in: ['params'],
      errorMessage: (val) => `The department with ID: ${val} is not deleted`,
      custom: {
        errorMessage: errorMessage.idNotDeleted,
        async options(id) {
          const response = await models.department.findOne({
            where: { id, deletedAt: { [models.Op.ne]: null } },
            paranoid: false,
          });
          return !!response;
        },
      },
    },
  }),

  getAll: checkSchema({
  }),
});
