import * as controller from './controller';
import validator from './validator';
import authentication from '../../utils/auth';

export default (router) => {
  // ----------------------------------------------------------------------------------------------
  // router.post('/token', authentication, validator.addToken, controller.addToken);
  // ----------------------------------------------------------------------------------------------
  // router.post('/', validator.logIn, controller.logIn);
  // ----------------------------------------------------------------------------------------------
  router.post('/registration',authentication, validator.register, controller.register);
};
