import coreRepository from '../../core/repository';
import models from '../../models';
import { RepositoryError } from '../../utils/error_handler';
import { createHashPassword} from '../../utils/crypto_util';

const repository = Object.create(coreRepository);

export default Object.assign(repository, {
  modelName: 'auth',

  async findUser(data) {
    try {
      const where = { $or: [] };
      if (data.phone) {
        where.$or.push({ value: { $eq: data.phone } },{ type: { $eq: 'phone' } });
      } else if (data.email) {
        where.$or.push({ value: { $eq: data.email } },{ type: { $eq: 'email' } });
      }
      const response = await models.auth.findOne({
        where,
        attributes: { include: ['user_id'] },
      });
      return response;
    } catch (error) {
      throw new RepositoryError(error);
    }
  },
  async newUser(data,modelName) {
    this.modelName = modelName
    let password = Math.random().toString(36).substring(6);
    // console.log('...........................password....................................',password)
    // console.log('...........................this.modelName....................................',this.modelName)
    // console.log('...........................insertData....................................',insertData)
    // try {
    //
    //   return repository.create({
    //     insertData,
    //   });
    // } catch (error) {
    //   throw new RepositoryError(error);
    // }
  },
});
