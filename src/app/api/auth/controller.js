import service from './service';
import cw from '../../core/controller';

export const addToken = cw((req) => service.addToken(req.user.id, req.body));

export const logIn = cw((req) => service.logIn(req.body));

export const register = cw((req) => service.register(req.body));
