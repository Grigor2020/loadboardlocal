import { checkSchema } from 'express-validator/check';

import { coreValidator } from '../../core/validator';

const validator = Object.create(coreValidator);

export default Object.assign(validator, {
  logIn: checkSchema({
    username: {
      in: ['body'],
      optional: true,
      isLength: {
        errorMessage: 'The username must be from 6 to 20 characters length',
        options: { min: 6, max: 20 },
      },
      custom: {
        errorMessage: 'One of username/email is required',
        async options(username, req) {
          if (username && req.req.body.email) return false;
          if (!username && !req.req.body.email) return false;
          return true;
        },
      },
    },
    email: {
      in: ['body'],
      optional: true,
      isEmail: true,
      errorMessage: 'The email must be a valid email',
      custom: {
        errorMessage: 'One of username/email is required',
        async options(email, req) {
          if (email && req.req.body.username) return false;
          if (!email && !req.req.body.username) return false;
          return true;
        },
      },
    },
    password: {
      in: ['body'],
      errorMessage: 'The password is required',
      isEmpty: { negated: true },
      isLength: {
        errorMessage: 'The password must be min 6 characters length',
        options: { min: 6 },
      },
    },
  }),
  
  register: checkSchema({
    firstName: {
      in: ['body'],
      errorMessage: 'The firstName is required',
      isEmpty: { negated: true },
    },
    lastName: {
      in: ['body'],
      errorMessage: 'The lastName is required',
      isEmpty: { negated: true },
    },
    email: {
      in: ['body'],
      optional: true,
      isEmail: true,
      errorMessage: 'The email must be a valid email',
      custom: {
        errorMessage: 'One of username/email is required',
        async options(email, req) {
          if (!email && !req.req.body.phone) return false;
          return true;
        },
      },
    },
    phone: {
      in: ['body'],
      optional: true,
      errorMessage: 'The phone number  must be a valid',
      custom: {
        errorMessage: 'One of username/email is required',
        async options(phone, req) {
          if (!phone && !req.req.body.email) return false;
          return true;
        },
      },
    }
  }),

  addToken: checkSchema({
    deviceToken: {
      in: ['body'],
      isLength: {
        errorMessage: 'The firebase token must be max 256 length',
        options: { max: 256 },
      },
      matches: {
        options: [/[0-9a-zA-Z\-\\_]/],
        errorMessage: 'The token must be with /[0-9a-zA-Z-\\_]/ pattern',
      },
    },
    devicePlatform: {
      in: ['body'],
      matches: {
        options: [/iOS|Android|Web|Other/],
        errorMessage: 'The platform must be one of iOS/Android/Web/Other',
      },
    },
  }),
});
