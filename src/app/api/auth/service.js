import uuid from 'uuid/v4';
import { get as redisGet } from '../../utils/redis';
import coreService from '../../core/service';
import repository from './repository';
import { ServiceError, AuthError } from '../../utils/error_handler';
import { verify, encrypt } from '../../utils/crypto_util';

const service = Object.create(coreService);

export default Object.assign(service, {
  /**
   *  Add token
   *  @param {Object} data : resource data object
   *  @return {Object} resource ORM object
   */
  async addToken(userId, data) {
    try {
      const { devicePlatform: platform, deviceToken: token } = data;
      await repository.create({
        userId,
        platform,
        token,
      }, {}, 'firebaseToken');
      return;
    } catch (error) {
      throw new ServiceError(error);
    }
  },

  /**
   *  Login user
   *  @param {Object} data : resource data object
   *  @return {Object} resource ORM object
   */
  async logIn(data) {
    try {
      let user = await repository.findUser(data);

      if (!user) {
        throw new AuthError('The username or email is wrong');
      }

      const verifies = verify(data.password, user.password, user.id);
      if (!verifies) {
        throw new AuthError('The password is wrong');
      }

      user = await repository.findByPk(user.id, { include: ['department', 'role'] });
      const token = encrypt(user.id);
      const redisData = await redisGet(token);
      const sessionId = uuid();

      redisData.user = user;
      if (redisData.sessions) {
        redisData.sessions.push(sessionId);
      } else redisData.sessions = [sessionId];

      global.redisClient.set(token, JSON.stringify(redisData));

      return { token, sessionId };
    } catch (error) {
      throw new ServiceError(error);
    }
  }, 
  
  async register(data) {
    console.log('data;;;;;;;;;;;;;;;',data)
    try {
      let user = await repository.findUser(data);

      console.log('............usr.............',user)
      
      if (user) {
        throw new AuthError('The username or email is exists');
      }
      
      // let newUser = await repository.newUser(data,'user')
      
      // const verifies = verify(data.password, user.password, user.id);
      // if (!verifies) {
      //   throw new AuthError('The password is wrong');
      // }
      //
      // user = await repository.findByPk(user.id, { include: ['department', 'role'] });
      // const token = encrypt(user.id);
      // const redisData = await redisGet(token);
      // const sessionId = uuid();
      //
      // redisData.user = user;
      // if (redisData.sessions) {
      //   redisData.sessions.push(sessionId);
      // } else redisData.sessions = [sessionId];
      // //
      // // global.redisClient.set(token, JSON.stringify(redisData));
      //
      //
      // return { token, sessionId };
    } catch (error) {
      throw new ServiceError(error);
    }
  },

});
