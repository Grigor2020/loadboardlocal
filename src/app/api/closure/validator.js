import { checkSchema } from 'express-validator/check';

import models from '../../models';
import { coreValidator, errorMessage } from '../../core/validator';

const validator = Object.create(coreValidator);

const validationFields = {
  serialNumber: {
    in: ['body'],
    errorMessage: errorMessage.serialNumber,
    isEmpty: { negated: true },
  },
  inputsItem: {
    in: ['body'],
    isArray: { errorMessage: errorMessage.array },
    errorMessage: errorMessage.inputsItem,
    custom: {
      errorMessage: 'The items must be positive numbers',
      options: (a) => a.every((item) => {
        if (!Number(item) || Number(item) <= 0) return false;
        return true;
      }),
    },
  },
};

export default Object.assign(validator, {
  store: checkSchema({
    brandId: coreValidator.brandInBody,
    type: coreValidator.closureType,
    model: coreValidator.model,
    serialNumber: { ...validationFields.serialNumber, optional: true },
    partnerCode: { ...coreValidator.partnerCode, optional: true },
    'dimensions.width': { ...coreValidator.dimensionItem, optional: true },
    'dimensions.length': { ...coreValidator.dimensionItem, optional: true },
    'dimensions.height': { ...coreValidator.dimensionItem, optional: true },
    'dimensions.weight': { ...coreValidator.dimensionItem, optional: true },
    'inputs.top': { ...validationFields.inputsItem, optional: true },
    'inputs.bottom': { ...validationFields.inputsItem, optional: true },
    'inputs.left': { ...validationFields.inputsItem, optional: true },
    'inputs.right': { ...validationFields.inputsItem, optional: true },
    maxFibers: { ...coreValidator.integer, optional: true },
    maxFiberTrays: { ...coreValidator.integer, optional: true },
    fileIds: { ...coreValidator.fileArrayInBody, optional: true },
    tags: { ...coreValidator.tags, optional: true },
  }),

  upsert: checkSchema({
    id: {
      in: ['params'],
      errorMessage: (val) => `The resource with ID: ${val} is deleted`,
      custom: {
        errorMessage: errorMessage.idDeleted,
        async options(id) {
          const response = await models.closure.findOne({
            where: { id, deletedAt: { [models.Op.ne]: null } },
            paranoid: false,
          });
          return !response;
        },
      },
    },
    brandId: coreValidator.brandInBody,
    type: coreValidator.closureType,
    model: coreValidator.model,
    serialNumber: { ...validationFields.serialNumber, optional: true },
    partnerCode: { ...coreValidator.partnerCode, optional: true },
    'dimensions.width': { ...coreValidator.dimensionItem, optional: true },
    'dimensions.length': { ...coreValidator.dimensionItem, optional: true },
    'dimensions.height': { ...coreValidator.dimensionItem, optional: true },
    'dimensions.weight': { ...coreValidator.dimensionItem, optional: true },
    'inputs.top': { ...validationFields.inputsItem, optional: true },
    'inputs.bottom': { ...validationFields.inputsItem, optional: true },
    'inputs.left': { ...validationFields.inputsItem, optional: true },
    'inputs.right': { ...validationFields.inputsItem, optional: true },
    maxFibers: { ...coreValidator.integer, optional: true },
    maxFiberTrays: { ...coreValidator.integer, optional: true },
    fileIds: { ...coreValidator.fileArrayInBody, optional: true },
    tags: { ...coreValidator.tags, optional: true },
  }),

  update: checkSchema({
    id: coreValidator.closureId,
    brandId: { ...coreValidator.brandInBody, optional: true },
    type: { ...coreValidator.closureType, optional: true },
    model: { ...coreValidator.model, optional: true },
    serialNumber: { ...validationFields.serialNumber, optional: true },
    partnerCode: { ...coreValidator.partnerCode, optional: true },
    'dimensions.width': { ...coreValidator.dimensionItem, optional: true },
    'dimensions.length': { ...coreValidator.dimensionItem, optional: true },
    'dimensions.height': { ...coreValidator.dimensionItem, optional: true },
    'dimensions.weight': { ...coreValidator.dimensionItem, optional: true },
    'inputs.top': { ...validationFields.inputsItem, optional: true },
    'inputs.bottom': { ...validationFields.inputsItem, optional: true },
    'inputs.left': { ...validationFields.inputsItem, optional: true },
    'inputs.right': { ...validationFields.inputsItem, optional: true },
    maxFibers: { ...coreValidator.integer, optional: true },
    maxFiberTrays: { ...coreValidator.integer, optional: true },
    fileIds: { ...coreValidator.fileArrayInBody, optional: true },
    tags: { ...coreValidator.tags, optional: true },
  }),

  isExist: checkSchema({
    id: coreValidator.closureId,
  }),

  getAll: checkSchema({
    offset: { ...coreValidator.offset, optional: true },
    limit: { ...coreValidator.limit, optional: true },
    include: {
      ...coreValidator.queryString,
      optional: true,
      custom: {
        errorMessage: 'The include and exclude fields cannot exist the same time',
        options: (include, { req }) => {
          if (req.query.exclude) return false;
          return true;
        },
      },
    },
    exclude: {
      ...coreValidator.queryString,
      optional: true,
      custom: {
        errorMessage: 'The include and exclude fields cannot exist the same time',
        options: (exclude, { req }) => {
          if (req.query.include) return false;
          return true;
        },
      },
    },
  }),

  isDeleted: checkSchema({
    id: {
      in: ['params'],
      errorMessage: (val) => `The resource with ID: ${val} is not deleted`,
      custom: {
        errorMessage: errorMessage.idNotDeleted,
        async options(id) {
          const response = await models.closure.findOne({
            where: { id, deletedAt: { [models.Op.ne]: null } },
            paranoid: false,
          });
          return !!response;
        },
      },
    },
  }),
});
