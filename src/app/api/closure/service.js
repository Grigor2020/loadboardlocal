import coreService from '../../core/service';
import repository from './repository';
import { ServiceError } from '../../utils/error_handler';
import destructQuery from '../../utils/helper';

const service = Object.create(coreService);

export default Object.assign(service, {
  /**
   *  Get All available resources
   *  @return {Array} array of resource ORM objects
   */
  getAll(query) {
    try {
      const where = { };
      const options = destructQuery(query, ['brand', 'files']);
      return repository.findAll(where, options);
    } catch (error) {
      throw new ServiceError(error);
    }
  },

  /**
   *  Get One available resources
   *  @param {Number} pk : resource ID
   *  @return {Object} resource ORM object
   */
  getByPk(pk) {
    try {
      return repository.findByPk(pk, { include: ['brand', 'files'] });
    } catch (error) {
      throw new ServiceError(error);
    }
  },

  /**
   *  Create resources
   *  @param {Object} data : resource data object
   *  @return {Object} resource ORM object
   */
  async store(data) {
    try {
      const created = await repository.create({
        ...data,
      });
      if (created) created.addFiles(data.fileIds);
      created.setDataValue('brand', await created.getBrand());
      created.setDataValue('files', await created.getFiles());
      return created;
    } catch (error) {
      throw new ServiceError(error);
    }
  },

  /**
   *  Update or Create resources
   *  @param {Object} data : resource data object
   *  @param {Object} id : resource ID
   *  @return {Object} resource ORM object
   */
  async upsert(data, id) {
    try {
      const upserted = await repository.upsert({
        ...data,
        id,
      });
      if (upserted) upserted.addFiles(data.fileIds);
      upserted.setDataValue('brand', await upserted.getBrand());
      upserted.setDataValue('files', await upserted.getFiles());
      return upserted;
    } catch (error) {
      throw new ServiceError(error);
    }
  },

  /**
   *  Update resources
   *  @param {Object} data : resource data object
   *  @param {Object} id : resource ID
   *  @return {Object} resource ORM object
   */
  async update(data, id) {
    try {
      const updated = await repository.update(
        data,
        { id },
      );
      if (updated) updated.addFiles(data.fileIds);
      return repository.findByPk(id, { include: ['brand', 'files'] });
    } catch (error) {
      throw new ServiceError(error);
    }
  },

  /**
   *  Remove resources
   *  @param {Object} id : resource ID
   *  @return {Object} resource ORM object
   */
  remove(id) {
    try {
      return repository.softDelete({ id });
    } catch (error) {
      throw new ServiceError();
    }
  },

  /**
   *  Revert resources
   *  @param {Object} id : resource ID
   *  @return {Object} resource ORM object
   */
  async revert(id) {
    try {
      await repository.revert({ id });
      return repository.findByPk(id, { include: ['brand', 'files'] });
    } catch (error) {
      throw new ServiceError();
    }
  },
});
