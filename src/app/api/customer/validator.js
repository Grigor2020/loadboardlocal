import { checkSchema } from 'express-validator/check';

import models from '../../models';
import { coreValidator, errorMessage } from '../../core/validator';

const validator = Object.create(coreValidator);

export default Object.assign(validator, {
  store: checkSchema({
  }),

  upsert: checkSchema({
    id: {
      in: ['params'],
      errorMessage: (val) => `The resource with ID: ${val} is deleted`,
      custom: {
        errorMessage: errorMessage.idDeleted,
        async options(id) {
          const response = await models.customer.findOne({
            where: { id, deletedAt: { [models.Op.ne]: null } },
            paranoid: false,
          });
          return !response;
        },
      },
    },
  }),

  update: checkSchema({
    id: coreValidator.customerId,
  }),

  isExist: checkSchema({
    id: coreValidator.customerId,
  }),

  getAll: checkSchema({
  }),

  isDeleted: checkSchema({
    id: {
      in: ['params'],
      errorMessage: (val) => `The resource with ID: ${val} is not deleted`,
      custom: {
        errorMessage: errorMessage.idNotDeleted,
        async options(id) {
          const response = await models.customer.findOne({
            where: { id, deletedAt: { [models.Op.ne]: null } },
            paranoid: false,
          });
          return !!response;
        },
      },
    },
  }),
});
