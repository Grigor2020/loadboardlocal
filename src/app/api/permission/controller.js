import service from './service';
import cw from '../../core/controller';

export const getAll = cw((req) => service.getAll(req.query));

export const get = cw((req) => service.getByPk(req.params.name));

export const store = cw((req) => service.store(req.body));

export const update = cw((req) => service.update(req.body, req.params.name));

export const remove = cw((req) => service.remove(req.params.name));

export const revert = cw((req) => service.revert(req.params.name));
