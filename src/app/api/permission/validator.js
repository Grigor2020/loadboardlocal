import { checkSchema } from 'express-validator/check';

import models from '../../models';
import { coreValidator, isExistIncludingDeleted, isResourceExist } from '../../core/validator';

const validator = Object.create(coreValidator);

const validationFields = {
  name: {
    in: ['body'],
    errorMessage: 'The permission name is required',
    custom: {
      options: (name) => isExistIncludingDeleted(name, 'name', models.permission),
      errorMessage: (val) => `A permission with name: ${val} already exists`,
    },
  },
  paramsName: {
    in: ['params'],
    errorMessage: 'The permission name is required',
    custom: {
      options: (name) => isResourceExist(name, models.permission),
      errorMessage: (val) => `A permission with name: ${val} does not exist`,
    },
  },
};

export default Object.assign(validator, {
  store: checkSchema({
    name: validationFields.name,
    description: { ...coreValidator.description, optional: true },
  }),

  update: checkSchema({
    name: validationFields.paramsName,
    description: { ...coreValidator.description, optional: true },
  }),

  isExist: checkSchema({
    name: validationFields.paramsName,
  }),

  isDeleted: checkSchema({
    name: {
      in: ['params'],
      errorMessage: 'The name is required',
      custom: {
        errorMessage: (val) => `The permission with name: ${val} is not deleted`,
        async options(name) {
          const response = await models.permission.findOne({
            where: { name, deletedAt: { [models.Op.ne]: null } },
            paranoid: false,
          });
          return !!response;
        },
      },
    },
  }),

  getAll: checkSchema({
  }),
});
