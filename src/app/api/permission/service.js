import coreService from '../../core/service';
import repository from './repository';
import { ServiceError } from '../../utils/error_handler';

const service = Object.create(coreService);

export default Object.assign(service, {
  /**
   *  Get All available resources
   *  @return {Array} array of resource ORM objects
   */
  getAll() {
    try {
      return repository.findAll({});
    } catch (error) {
      throw new ServiceError(error);
    }
  },

  /**
   *  Get One available resources
   *  @param {String} name : resource pk
   *  @return {Object} resource ORM object
   */
  getByPk(pk) {
    try {
      return repository.findByPk(pk);
    } catch (error) {
      throw new ServiceError(error);
    }
  },

  /**
   *  Create resources
   *  @param {Object} data : resource data object
   *  @return {Object} resource ORM object
   */
  store(data) {
    try {
      return repository.create({
        ...data,
      });
    } catch (error) {
      throw new ServiceError(error);
    }
  },

  /**
   *  Update resources
   *  @param {Object} data : resource data object
   *  @param {String} name : resource PK
   *  @return {Object} resource ORM object
   */
  update(data, name) {
    try {
      return repository.update(
        data,
        { name },
      );
    } catch (error) {
      throw new ServiceError(error);
    }
  },

  /**
   *  Remove resources
   *  @param {String} name : resource PK
   *  @return {Object} resource ORM object
   */
  remove(name) {
    try {
      return repository.softDelete({ name });
    } catch (error) {
      throw new ServiceError();
    }
  },

  /**
   *  Revert resources
   *  @param {String} name : resource PK
   *  @return {Object} resource ORM object
   */
  async revert(name) {
    try {
      await repository.revert({ name });
      return repository.findByPk(name);
    } catch (error) {
      throw new ServiceError();
    }
  },
});
