import * as controller from './controller';
import validator from './validator';

export default (router) => {
  // ----------------------------------------------------------------------------------------------
  router.get('/', validator.getAll, controller.getAll);
  // ----------------------------------------------------------------------------------------------
  router.get('/:name', validator.isExist, controller.get);
  // ----------------------------------------------------------------------------------------------
  router.post('/', validator.store, controller.store);
  // ----------------------------------------------------------------------------------------------
  router.patch('/:name', validator.update, controller.update);
  // ----------------------------------------------------------------------------------------------
  router.delete('/:name', validator.isExist, controller.remove);
  // ----------------------------------------------------------------------------------------------
  router.patch('/revert/:name', validator.isDeleted, controller.revert);
};
