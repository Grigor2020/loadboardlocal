import uuid from 'uuid/v4';
import coreService from '../../core/service';
import repository from './repository';
import { ServiceError } from '../../utils/error_handler';
import destructQuery from '../../utils/helper';
import { createHash } from '../../utils/crypto_util';

const service = Object.create(coreService);

export default Object.assign(service, {
  /**
   *  Get All available resources
   *  @return {Array} array of resource ORM objects
   */
  getAll(query) {
    try {
      const where = { };
      const options = destructQuery(query, ['role', 'profilePicture', 'country', 'department']);
      return repository.findAll(where, options);
    } catch (error) {
      throw new ServiceError(error);
    }
  },

  /**
   *  Get One available resources
   *  @param {Number} pk : resource ID
   *  @return {Object} resource ORM object
   */
  getByPk(pk) {
    try {
      return repository.findByPk(pk, { include: ['country', 'profilePicture', 'role', 'department'] });
    } catch (error) {
      throw new ServiceError(error);
    }
  },

  /**
   *  Create resources
   *  @param {Object} data : resource data object
   *  @return {Object} resource ORM object
   */
  async store(data) {
    try {
      const id = uuid();
      const transaction = await repository.startTransaction();
      const created = await repository.create({
        id,
        ...data,
      }, { transaction });
      const hashPassword = createHash(data.password, created.id);
      await repository.update({ password: hashPassword }, { id: created.id }, { transaction });

      created.setDataValue('role', await created.getRole());
      created.setDataValue('profilePicture', await created.getProfilePicture());
      created.setDataValue('department', await created.getDepartment());
      created.setDataValue('country', await created.getCountry());
      await repository.commitTransaction(transaction);
      return created;
    } catch (error) {
      throw new ServiceError(error);
    }
  },

  /**
   *  Update or Create resources
   *  @param {Object} data : resource data object
   *  @param {Object} id : resource ID
   *  @return {Object} resource ORM object
   */
  async upsert(data, id) {
    try {
      const transaction = await repository.startTransaction();
      const upserted = await repository.upsert({
        ...data,
        id,
      }, { transaction });
      const hashPassword = createHash(data.password, upserted.id);
      await repository.update({ password: hashPassword }, { id: upserted.id }, { transaction });

      upserted.setDataValue('role', await upserted.getRole());
      upserted.setDataValue('profilePicture', await upserted.getProfilePicture());
      upserted.setDataValue('department', await upserted.getDepartment());
      upserted.setDataValue('country', await upserted.getCountry());
      await repository.commitTransaction(transaction);
      return upserted;
    } catch (error) {
      throw new ServiceError(error);
    }
  },

  /**
   *  Update resources
   *  @param {Object} data : resource data object
   *  @param {Object} id : resource ID
   *  @return {Object} resource ORM object
   */
  async update(data, id) {
    try {
      if (data.password) {
        // eslint-disable-next-line no-param-reassign
        data.password = createHash(data.password, id);
      }
      await repository.update(
        data,
        { id },
      );
      return repository.findByPk(id, { include: ['country', 'department', 'profilePicture', 'role'] });
    } catch (error) {
      throw new ServiceError(error);
    }
  },

  /**
   *  Remove resources
   *  @param {Object} id : resource ID
   *  @return {Object} resource ORM object
   */
  remove(id) {
    try {
      return repository.softDelete({ id });
    } catch (error) {
      throw new ServiceError();
    }
  },

  /**
   *  Revert resources
   *  @param {Object} id : resource ID
   *  @return {Object} resource ORM object
   */
  async revert(id) {
    try {
      await repository.revert({ id });
      return repository.findByPk(id, { include: ['country', 'department', 'profilePicture', 'role'] });
    } catch (error) {
      throw new ServiceError();
    }
  },
});
