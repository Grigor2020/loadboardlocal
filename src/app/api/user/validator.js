import { checkSchema } from 'express-validator/check';

import models from '../../models';
import { coreValidator, errorMessage, isResourceExist } from '../../core/validator';

const validator = Object.create(coreValidator);

const validationFields = {
  firstName: {
    in: ['body'],
    errorMessage: 'The firstName is required',
    isEmpty: { negated: true },
  },
  lastName: {
    in: ['body'],
    errorMessage: 'The lastName is required',
    isEmpty: { negated: true },
  },
  boolean: {
    in: ['body'],
    errorMessage: 'The field must be boolean',
    optional: true,
    isBoolean: true,
  },
  username: {
    in: ['body'],
    errorMessage: 'The username is required',
    isEmpty: { negated: true },
    isLength: {
      errorMessage: 'The username must be from 6 to 20 characters length',
      options: { min: 6, max: 20 },
    },
    custom: {
      errorMessage: 'The username is already used by another user',
      async options(username) {
        const response = await models.user.findOne({
          where: { username },
          paranoid: false,
        });
        return !response;
      },
    },
  },
  password: {
    in: ['body'],
    errorMessage: 'The password is required',
    isEmpty: { negated: true },
    isLength: {
      errorMessage: 'The password must be min 6 characters length',
      options: { min: 6 },
    },
  },
  day: {
    in: ['body'],
    optional: true,
    matches: {
      optional: true,
      errorMessage: 'The date must be YYYY-MM-DD format',
      options: [/^\d{4}-(0?[1-9]|1[012])-(0?[1-9]|[12][0-9]|3[01])$/],
    },
  },
  gender: {
    in: ['body'],
    optional: true,
    matches: {
      options: [/\b(?:MALE|FEMALE|NA)\b/],
      errorMessage: 'The gender must be one of MALE/FEMALE/NA',
    },
  },
  permission: {
    in: ['body'],
    errorMessage: 'The permission is required',
    optional: true,
    matches: {
      options: [/[0-9]{1,}/],
      errorMessage: 'The permission value can be only digits',
    },
  },
  id: {
    in: ['params'],
    errorMessage: (val) => `The user with ID: ${val} does not exist`,
    custom: { options: (id) => isResourceExist(id, models.user) },
  },
};

export default Object.assign(validator, {
  store: checkSchema({
    firstName: validationFields.firstName,
    lastName: validationFields.lastName,
    username: validationFields.username,
    password: validationFields.password,
    phone: coreValidator.phone,
    address: { ...coreValidator.address, optional: true },
    birthday: validationFields.day,
    gender: validationFields.gender,
    email: { ...coreValidator.email, optional: true },
    permission: validationFields.permission,
    countryId: { ...coreValidator.countryInBody, optional: true },
    departmentId: { ...coreValidator.departmentInBody, optional: true },
    roleId: { ...coreValidator.roleInBody, optional: true },
    profilePictureId: { ...coreValidator.fileInBody, optional: true },
  }),

  upsert: checkSchema({
    id: {
      in: ['params'],
      errorMessage: (val) => `The user with ID: ${val} is deleted`,
      custom: {
        errorMessage: errorMessage.idDeleted,
        async options(id) {
          const response = await models.user.findOne({
            where: { id, deletedAt: { [models.Op.ne]: null } },
            paranoid: false,
          });
          return !response;
        },
      },
    },
    firstName: validationFields.firstName,
    lastName: validationFields.lastName,
    username: {
      in: ['body'],
      errorMessage: 'The username is required',
      optional: true,
      isLength: {
        errorMessage: 'The username must be from 6 to 20 characters length',
        options: { min: 6, max: 20 },
      },
      custom: {
        errorMessage: 'The username is already used by another user',
        async options(username, req) {
          const response = await models.user.findOne({
            where: { username },
            paranoid: false,
          });
          if (response && (response.dataValues.id === Number(req.req.params.id))) return true;
          return !response;
        },
      },
    },
    password: validationFields.password,
    phone: coreValidator.phone,
    address: { ...coreValidator.address, optional: true },
    birthday: validationFields.day,
    gender: validationFields.gender,
    email: { ...coreValidator.email, optional: true },
    permission: validationFields.permission,
    countryId: { ...coreValidator.countryInBody, optional: true },
    departmentId: { ...coreValidator.departmentInBody, optional: true },
    roleId: { ...coreValidator.roleInBody, optional: true },
    profilePictureId: { ...coreValidator.fileInBody, optional: true },
  }),

  update: checkSchema({
    id: validationFields.id,
    firstName: { ...validationFields.firstName, optional: true },
    lastName: { ...validationFields.lastName, optional: true },
    username: {
      in: ['body'],
      errorMessage: 'The username is required',
      optional: true,
      isLength: {
        errorMessage: 'The username must be from 6 to 20 characters length',
        options: { min: 6, max: 20 },
      },
      custom: {
        errorMessage: 'The username is already used by another user',
        async options(username, req) {
          const response = await models.user.findOne({
            where: { username },
            paranoid: false,
          });
          if (response && (response.dataValues.id === Number(req.req.params.id))) return true;
          return !response;
        },
      },
    },
    password: { ...validationFields.password, optional: true },
    phone: { ...coreValidator.phone, optional: true },
    address: { ...coreValidator.address, optional: true },
    birthday: validationFields.day,
    gender: validationFields.gender,
    email: { ...coreValidator.email, optional: true },
    permission: validationFields.permission,
    isVerified: validationFields.boolean,
    isBlocked: validationFields.boolean,
    countryId: { ...coreValidator.countryInBody, optional: true },
    departmentId: { ...coreValidator.departmentInBody, optional: true },
    roleId: { ...coreValidator.roleInBody, optional: true },
    profilePictureId: { ...coreValidator.fileInBody, optional: true },
  }),

  isExist: checkSchema({
    id: validationFields.id,
  }),

  isDeleted: checkSchema({
    id: {
      in: ['params'],
      errorMessage: (val) => `The resource with ID: ${val} is not deleted`,
      custom: {
        errorMessage: errorMessage.idNotDeleted,
        async options(id) {
          const response = await models.user.findOne({
            where: { id, deletedAt: { [models.Op.ne]: null } },
            paranoid: false,
          });
          return !!response;
        },
      },
    },
  }),

  getAll: checkSchema({
  }),
});
