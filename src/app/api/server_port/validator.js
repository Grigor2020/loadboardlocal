import { checkSchema } from 'express-validator/check';
import { coreValidator } from '../../core/validator';

const validator = Object.create(coreValidator);

export default Object.assign(validator, {
  store: checkSchema({
  }),

  upsert: checkSchema({
  }),

  update: checkSchema({
  }),

  isExist: checkSchema({
  }),

  getAll: checkSchema({
  }),
});
