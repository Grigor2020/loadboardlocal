import service from './service';
import cw from '../../core/controller';

export const getAll = cw(() => service.getAll());

export const get = cw((req) => service.getByPk(req.params.id));

export const store = cw((req) => service.store(req.body));

export const upsert = cw((req) => service.upsert(req.body, req.params.id));

export const update = cw((req) => service.update(req.body, req.params.id));

export const remove = cw((req) => service.remove(req.params.id));
