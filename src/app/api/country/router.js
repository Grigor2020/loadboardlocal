import * as controller from './controller';
import validator from './validator';

export default (router) => {
  // ----------------------------------------------------------------------------------------------
  router.get('/', validator.getAll, controller.getAll);
  // ----------------------------------------------------------------------------------------------
  router.get('/:id', validator.isExist, controller.get);
};
