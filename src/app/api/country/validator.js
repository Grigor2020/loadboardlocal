import { checkSchema } from 'express-validator/check';

import { coreValidator } from '../../core/validator';

const validator = Object.create(coreValidator);

export default Object.assign(validator, {
  isExist: checkSchema({
    id: coreValidator.countryId,
  }),

  getAll: checkSchema({
    name: coreValidator.nameQuery,
    offset: coreValidator.offset,
    limit: coreValidator.limit,
  }),
});
