import { checkSchema } from 'express-validator/check';

import models from '../../models';
import { coreValidator, errorMessage } from '../../core/validator';

const validator = Object.create(coreValidator);

const validationFields = {
  model: {
    in: ['body'],
    errorMessage: 'The model is required',
    custom: {
      errorMessage: 'One of model/serialNumber fields is required',
      options: (model, { req }) => {
        if (!model && !req.body.serialNumber) return false;
        return true;
      },
    },
  },
  serialNumber: {
    in: ['body'],
    errorMessage: 'The serialNumber is required',
    custom: {
      errorMessage: 'One of model/serialNumber fields is required',
      options: (serialNumber, { req }) => {
        if (!serialNumber && !req.body.model) return false;
        return true;
      },
    },
  },
  bundleCount: {
    in: ['body'],
    isInt: {
      options: { min: 1, max: 128 },
      errorMessage: 'The field must be from 1 to 128 integer',
    },
  },
  fiberCount: {
    in: ['body'],
    isInt: {
      options: { min: 1, max: 128 },
      errorMessage: 'The field must be from 1 to 128 integer',
    },
  },
  sag: {
    in: ['body'],
    isFloat: {
      options: { min: 0, max: 100 },
      errorMessage: 'The field must be from 0 to 100 number',
    },
  },
  bundleColors: {
    in: ['body'],
    isArray: { errorMessage: errorMessage.array },
    errorMessage: 'The bundle colors are required',
    custom: {
      errorMessage: 'The bundleColors must have color and line fields',
      options: (a) => a.every((item) => {
        if (!item.color || !/^#[A-Fa-f0-9]{6}$/.test(item.color)) return false;
        if (!item.line.toString() || !/^[012]{1}$/.test(item.line)) return false;
        return true;
      }),
    },
  },
  fiberColors: {
    in: ['body'],
    isArray: { errorMessage: errorMessage.array },
    errorMessage: 'The fiber colors are required',
    custom: {
      errorMessage: 'The fiberColors must have color and line fields',
      options: (a) => a.every((item) => {
        if (!item.color || !/^#[A-Fa-f0-9]{6}$/.test(item.color)) return false;
        if (!item.line.toString() || !/^[012]{1}$/.test(item.line)) return false;
        return true;
      }),
    },
  },
  loss: {
    in: ['body'],
    errorMessage: 'The loss is required',
    isArray: { errorMessage: errorMessage.array },
    custom: {
      errorMessage: 'The loss must be array of objects with nm and value keys',
      options: (a) => a.every((item) => {
        if (!item.nm || !Number(item.nm)) return false;
        if (!item.value || !Number(item.value)) return false;
        return true;
      }),
    },
  },
};

export default Object.assign(validator, {
  store: checkSchema({
    brandId: coreValidator.brandInBody,
    type: coreValidator.cableType,
    model: validationFields.model,
    serialNumber: validationFields.serialNumber,
    partnerCode: { ...coreValidator.partnerCode, optional: true },
    bundleCount: { ...validationFields.bundleCount, optional: true },
    bundleColors: { ...validationFields.bundleColors, optional: true },
    fiberCount: { ...validationFields.fiberCount, optional: true },
    fiberColors: validationFields.fiberColors,
    loss: validationFields.loss,
    weight: { ...coreValidator.positiveNumber, optional: true },
    sag: { ...validationFields.sag, optional: true },
    span: { ...coreValidator.number, optional: true },
    tensionLoad: { ...coreValidator.number, optional: true },
    diameter: { ...coreValidator.number, optional: true },
    fileIds: { ...coreValidator.fileArrayInBody, optional: true },
  }),

  upsert: checkSchema({
    id: {
      in: ['params'],
      errorMessage: (val) => `The resource with ID: ${val} is deleted`,
      custom: {
        errorMessage: errorMessage.idDeleted,
        async options(id) {
          const response = await models.cable.findOne({
            where: { id, deletedAt: { [models.Op.ne]: null } },
            paranoid: false,
          });
          return !response;
        },
      },
    },
    brandId: coreValidator.brandInBody,
    type: coreValidator.cableType,
    model: validationFields.model,
    serialNumber: validationFields.serialNumber,
    partnerCode: { ...coreValidator.partnerCode, optional: true },
    bundleCount: { ...validationFields.bundleCount, optional: true },
    bundleColors: { ...validationFields.bundleColors, optional: true },
    fiberCount: { ...validationFields.fiberCount, optional: true },
    fiberColors: validationFields.fiberColors,
    loss: validationFields.loss,
    weight: { ...coreValidator.positiveNumber, optional: true },
    sag: { ...validationFields.sag, optional: true },
    span: { ...coreValidator.number, optional: true },
    tensionLoad: { ...coreValidator.number, optional: true },
    diameter: { ...coreValidator.number, optional: true },
    fileIds: { ...coreValidator.fileArrayInBody, optional: true },
  }),

  update: checkSchema({
    id: coreValidator.cableId,
    brandId: { ...coreValidator.brandInBody, optional: true },
    type: { ...coreValidator.cableType, optional: true },
    model: {
      in: ['body'],
      optional: true,
    },
    serialNumber: {
      in: ['body'],
      optional: true,
    },
    partnerCode: { ...coreValidator.partnerCode, optional: true },
    bundleCount: { ...validationFields.bundleCount, optional: true },
    bundleColors: { ...validationFields.bundleColors, optional: true },
    fiberCount: { ...validationFields.fiberCount, optional: true },
    fiberColors: { ...validationFields.fiberColors, optional: true },
    loss: { ...validationFields.loss, optional: true },
    weight: { ...coreValidator.positiveNumber, optional: true },
    sag: { ...validationFields.sag, optional: true },
    span: { ...coreValidator.number, optional: true },
    tensionLoad: { ...coreValidator.number, optional: true },
    diameter: { ...coreValidator.number, optional: true },
    fileIds: { ...coreValidator.fileArrayInBody, optional: true },
  }),

  isExist: checkSchema({
    id: coreValidator.cableId,
  }),

  isDeleted: checkSchema({
    id: {
      in: ['params'],
      errorMessage: (val) => `The resource with ID: ${val} is not deleted`,
      custom: {
        errorMessage: errorMessage.idNotDeleted,
        async options(id) {
          const response = await models.cable.findOne({
            where: { id, deletedAt: { [models.Op.ne]: null } },
            paranoid: false,
          });
          return !!response;
        },
      },
    },
  }),

  getAll: checkSchema({
    offset: { ...coreValidator.offset, optional: true },
    limit: { ...coreValidator.limit, optional: true },
    include: {
      ...coreValidator.queryString,
      optional: true,
      custom: {
        errorMessage: 'The include and exclude fields cannot exist the same time',
        options: (include, { req }) => {
          if (req.query.exclude) return false;
          return true;
        },
      },
    },
    exclude: {
      ...coreValidator.queryString,
      optional: true,
      custom: {
        errorMessage: 'The include and exclude fields cannot exist the same time',
        options: (exclude, { req }) => {
          if (req.query.include) return false;
          return true;
        },
      },
    },
  }),
});
