import { checkSchema } from 'express-validator/check';

import models from '../../models';
import { coreValidator, errorMessage } from '../../core/validator';

const validator = Object.create(coreValidator);

export default Object.assign(validator, {
  store: checkSchema({
  }),

  upsert: checkSchema({
    id: {
      in: ['params'],
      errorMessage: (val) => `The resource with ID: ${val} is deleted`,
      custom: {
        errorMessage: errorMessage.idDeleted,
        async options(id) {
          const response = await models.server.findByPk(id, { paranoid: false });
          return !!response;
        },
      },
    },
  }),

  update: checkSchema({
    id: coreValidator.serverId,
  }),

  isExist: checkSchema({
    id: coreValidator.serverId,
  }),

  getAll: checkSchema({
  }),
});
