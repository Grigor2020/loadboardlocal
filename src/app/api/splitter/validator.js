import { checkSchema } from 'express-validator/check';

import models from '../../models';
import { coreValidator, errorMessage } from '../../core/validator';

const validator = Object.create(coreValidator);

const validationFields = {
  fiberInQuantity: {
    in: ['body'],
    optional: true,
    isInt: {
      options: { min: 1, max: 2 },
      errorMessage: 'The quantity must be an integer from 1 to 2',
    },
  },
  fiberOutQuantity: {
    in: ['body'],
    optional: true,
    matches: {
      options: [/2|4|8|16|32|64/],
      errorMessage: 'The quantity must be one of 2/4/8/16/32/64',
    },
  },
  colors: {
    in: ['body'],
    isArray: { errorMessage: errorMessage.array },
    errorMessage: 'The colors field is required',
    custom: {
      errorMessage: 'The colors must have color and line fields',
      options: (a) => a.every((item) => {
        if (!item.color || !/^#[A-Fa-f0-9]{6}$/.test(item.color)) return false;
        if (!item.line.toString() || !/^[012]{1}$/.test(item.line)) return false;
        return true;
      }),
    },
  },
};

export default Object.assign(validator, {
  store: checkSchema({
    brandId: coreValidator.brandInBody,
    type: coreValidator.splitterType,
    model: { ...coreValidator.model, optional: true },
    partnerCode: { ...coreValidator.partnerCode, optional: true },
    fiberInQuantity: validationFields.fiberInQuantity,
    fiberOutQuantity: validationFields.fiberOutQuantity,
    outLoss: { ...coreValidator.positiveNumber, optional: true },
    secondaryOutLoss: { ...coreValidator.positiveNumber, optional: true },
    colors: validationFields.colors,
    fileId: { ...coreValidator.fileInBody, optional: true },
  }),

  upsert: checkSchema({
    id: {
      in: ['params'],
      errorMessage: (val) => `The resource with ID: ${val} is deleted`,
      custom: {
        errorMessage: errorMessage.idDeleted,
        async options(id) {
          const response = await models.splitter.findOne({
            where: { id, deletedAt: { [models.Op.ne]: null } },
            paranoid: false,
          });
          return !response;
        },
      },
    },
    brandId: coreValidator.brandInBody,
    type: coreValidator.splitterType,
    model: { ...coreValidator.model, optional: true },
    partnerCode: { ...coreValidator.partnerCode, optional: true },
    fiberInQuantity: validationFields.fiberInQuantity,
    fiberOutQuantity: validationFields.fiberOutQuantity,
    outLoss: { ...coreValidator.positiveNumber, optional: true },
    secondaryOutLoss: { ...coreValidator.positiveNumber, optional: true },
    colors: validationFields.colors,
    fileId: { ...coreValidator.fileInBody, optional: true },
  }),

  update: checkSchema({
    id: coreValidator.splitterId,
    brandId: { ...coreValidator.brandInBody, optional: true },
    type: { ...coreValidator.splitterType, optional: true },
    model: { ...coreValidator.model, optional: true },
    partnerCode: { ...coreValidator.partnerCode, optional: true },
    fiberInQuantity: validationFields.fiberInQuantity,
    fiberOutQuantity: validationFields.fiberOutQuantity,
    outLoss: { ...coreValidator.positiveNumber, optional: true },
    secondaryOutLoss: { ...coreValidator.positiveNumber, optional: true },
    colors: validationFields.colors,
    fileId: { ...coreValidator.fileInBodyNullable, optional: true },
  }),

  isExist: checkSchema({
    id: coreValidator.splitterId,
  }),

  getAll: checkSchema({
    offset: { ...coreValidator.offset, optional: true },
    limit: { ...coreValidator.limit, optional: true },
    include: {
      ...coreValidator.queryString,
      optional: true,
      custom: {
        errorMessage: 'The include and exclude fields cannot exist the same time',
        options: (include, { req }) => {
          if (req.query.exclude) return false;
          return true;
        },
      },
    },
    exclude: {
      ...coreValidator.queryString,
      optional: true,
      custom: {
        errorMessage: 'The include and exclude fields cannot exist the same time',
        options: (exclude, { req }) => {
          if (req.query.include) return false;
          return true;
        },
      },
    },
  }),

  isDeleted: checkSchema({
    id: {
      in: ['params'],
      errorMessage: (val) => `The resource with ID: ${val} is not deleted`,
      custom: {
        errorMessage: errorMessage.idNotDeleted,
        async options(id) {
          const response = await models.splitter.findOne({
            where: { id, deletedAt: { [models.Op.ne]: null } },
            paranoid: false,
          });
          return !!response;
        },
      },
    },
  }),
});
