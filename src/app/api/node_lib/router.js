import * as controller from './controller';
import validator from './validator';

export default (router) => {
  // ----------------------------------------------------------------------------------------------
  router.get('/', validator.getAll, controller.getAll);
  // ----------------------------------------------------------------------------------------------
  router.get('/:id', validator.isExist, controller.get);
  // ----------------------------------------------------------------------------------------------
  router.post('/', validator.store, controller.store);
  // ----------------------------------------------------------------------------------------------
  router.put('/:id', validator.upsert, controller.upsert);
  // ----------------------------------------------------------------------------------------------
  router.patch('/:id', validator.update, controller.update);
  // ----------------------------------------------------------------------------------------------
  router.delete('/:id', validator.isExist, controller.remove);
};
