import { checkSchema } from 'express-validator/check';
import { coreValidator, errorMessage } from '../../core/validator';
import models from '../../models';

const validator = Object.create(coreValidator);

export default Object.assign(validator, {
  store: checkSchema({
    serverId: { ...coreValidator.serverInBody, optional: true },
    closureId: { ...coreValidator.closureInBody, optional: true },
  }),

  upsert: checkSchema({
    id: {
      in: ['params'],
      errorMessage: (val) => `The resource with ID: ${val} does not exist`,
      custom: {
        errorMessage: errorMessage.idDeleted,
        async options(id) {
          const response = await models.nodeLib.findByPk(id, { paranoid: false });
          return !!response;
        },
      },
    },
    serverId: { ...coreValidator.serverInBody, optional: true },
    closureId: { ...coreValidator.closureInBody, optional: true },
  }),

  update: checkSchema({
    id: {
      in: ['params'],
      errorMessage: (val) => `The resource with ID: ${val} is deleted`,
      custom: {
        errorMessage: errorMessage.idDeleted,
        async options(id) {
          const response = await models.nodeLib.findByPk(id, { paranoid: false });
          return !!response;
        },
      },
    },
    serverId: {
      ...coreValidator.serverInBody,
      optional: { options: { nullable: true } },
    },
    closureId: {
      ...coreValidator.closureInBody,
      optional: { options: { nullable: true } },
    },
  }),

  isExist: checkSchema({
    id: coreValidator.nodeLibId,
  }),

  getAll: checkSchema({
  }),
});
