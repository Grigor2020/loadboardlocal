import gjv from 'geojson-validation';
import { checkSchema } from 'express-validator/check';

import models from '../../models';
import { coreValidator, errorMessage } from '../../core/validator';

const validator = Object.create(coreValidator);

export default Object.assign(validator, {
  store: checkSchema({
    geom: {
      in: ['body'],
      errorMessage: errorMessage.geom,
      isEmpty: { negated: true },
      custom: {
        errorMessage: errorMessage.jsonError,
        options: (json) => gjv.isLineString(json),
      },
    },
    startNodeId: coreValidator.nodeInBody,
    endNodeId: coreValidator.nodeInBody,
    cableId: coreValidator.cableInBody,
  }),

  upsert: checkSchema({
    id: {
      in: ['params'],
      errorMessage: (val) => `The resource with ID: ${val} is deleted`,
      custom: {
        errorMessage: errorMessage.idDeleted,
        async options(id) {
          const response = await models.edge.findByPk(id, { paranoid: false });
          return !!response;
        },
      },
    },
    geom: {
      in: ['body'],
      errorMessage: errorMessage.geom,
      isEmpty: { negated: true },
      custom: {
        errorMessage: errorMessage.jsonError,
        options: (json) => gjv.isLineString(json),
      },
    },
    startNodeId: coreValidator.nodeInBody,
    endNodeId: coreValidator.nodeInBody,
    cableId: coreValidator.cableInBody,
  }),

  update: checkSchema({
    id: coreValidator.edgeId,
    geom: {
      in: ['body'],
      errorMessage: errorMessage.geom,
      optional: true,
      custom: {
        errorMessage: errorMessage.jsonError,
        options: (json) => gjv.isLineString(json),
      },
    },
    startNodeId: { ...coreValidator.nodeInBody, optional: true },
    endNodeId: { ...coreValidator.nodeInBody, optional: true },
    cableId: { ...coreValidator.cableInBody, optional: true },
  }),

  isExist: checkSchema({
    id: coreValidator.edgeId,
  }),

  getAll: checkSchema({
  }),

  isDeleted: checkSchema({
    id: {
      in: ['params'],
      errorMessage: (val) => `The resource with ID: ${val} is not deleted`,
      custom: {
        errorMessage: errorMessage.idNotDeleted,
        async options(id) {
          const response = await models.edge.findOne({
            where: { id, deletedAt: { [models.Op.ne]: null } },
            paranoid: false,
          });
          return !!response;
        },
      },
    },
  }),
});
