import { checkSchema } from 'express-validator/check';

import models from '../../models';
import { coreValidator, errorMessage, isResourceExist } from '../../core/validator';

const validator = Object.create(coreValidator);

const validationFields = {
  id: {
    in: ['params'],
    errorMessage: (val) => `The role with ID: ${val} does not exist`,
    custom: { options: (id) => isResourceExist(id, models.role) },
  },
  name: {
    in: ['body'],
    errorMessage: 'The role name is required',
    isEmpty: { negated: true },
  },
  permission: {
    in: ['body'],
    errorMessage: 'The permission is required',
    isEmpty: { negated: true },
    matches: {
      options: [/[0-9]{1,}/],
      errorMessage: 'The permission value can be only digits',
    },
  },
};

export default Object.assign(validator, {
  store: checkSchema({
    name: validationFields.name,
    permission: validationFields.permission,
  }),

  upsert: checkSchema({
    id: {
      in: ['params'],
      errorMessage: (val) => `The role with ID: ${val} is deleted`,
      custom: {
        errorMessage: errorMessage.idDeleted,
        async options(id) {
          const response = await models.role.findOne({
            where: { id, deletedAt: { [models.Op.ne]: null } },
            paranoid: false,
          });
          return !response;
        },
      },
    },
    name: validationFields.name,
    permission: validationFields.permission,
  }),

  update: checkSchema({
    id: validationFields.id,
    name: { ...validationFields.name, optional: true },
    permission: { ...validationFields.permission, optional: true },
  }),

  isExist: checkSchema({
    id: validationFields.id,
  }),

  isDeleted: checkSchema({
    id: {
      in: ['params'],
      errorMessage: (val) => `The role with ID: ${val} is not deleted`,
      custom: {
        errorMessage: errorMessage.idNotDeleted,
        async options(id) {
          const response = await models.role.findOne({
            where: { id, deletedAt: { [models.Op.ne]: null } },
            paranoid: false,
          });
          return !!response;
        },
      },
    },
  }),

  getAll: checkSchema({
  }),
});
