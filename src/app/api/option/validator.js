import { checkSchema } from 'express-validator/check';

import models from '../../models';
import { coreValidator, errorMessage, isResourceExist } from '../../core/validator';

const validator = Object.create(coreValidator);

const validationFields = {
  id: {
    in: ['params'],
    errorMessage: (val) => `The option with ID: ${val} does not exist`,
    custom: { options: (id) => isResourceExist(id, models.option) },
  },
  value: {
    in: ['body'],
    errorMessage: 'The value is required',
    isEmpty: { negated: true },
    custom: {
      options: (value) => {
        if (!JSON.stringify(value)) return false;
        return true;
      },
    },
  },
  key: {
    in: ['body'],
    errorMessage: 'The key is required',
    isEmpty: { negated: true },
  },
};

export default Object.assign(validator, {
  store: checkSchema({
    value: validationFields.value,
    key: validationFields.key,
  }),

  upsert: checkSchema({
    id: {
      in: ['params'],
      errorMessage: (val) => `The resource with ID: ${val} is deleted`,
      custom: {
        errorMessage: errorMessage.idDeleted,
        async options(id) {
          const response = await models.option.findOne({
            where: { id, deletedAt: { [models.Op.ne]: null } },
            paranoid: false,
          });
          return !response;
        },
      },
    },
    value: validationFields.value,
    key: validationFields.key,
  }),

  update: checkSchema({
    id: validationFields.id,
    value: { ...validationFields.value, optional: true },
    key: { ...validationFields.key, optional: true },
  }),

  isExist: checkSchema({
    id: validationFields.id,
  }),

  isDeleted: checkSchema({
    id: {
      in: ['params'],
      errorMessage: (val) => `The option with ID: ${val} is not deleted`,
      custom: {
        errorMessage: errorMessage.idNotDeleted,
        async options(id) {
          const response = await models.option.findOne({
            where: { id, deletedAt: { [models.Op.ne]: null } },
            paranoid: false,
          });
          return !!response;
        },
      },
    },
  }),

  getAll: checkSchema({
  }),
});
