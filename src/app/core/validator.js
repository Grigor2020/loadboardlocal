import models from '../models';

export const isExistByParameter = async (search, by, repository) => {
  const response = await repository.findOne({ where: { [by]: search } });
  return !response;
};

export const isExistIncludingDeleted = async (search, by, repository) => {
  const response = await repository.findOne({ where: { [by]: search }, paranoid: false });
  return !response;
};

export const isResourceExist = async (pk, repository) => {
  const response = await repository.findByPk(pk);
  return !!response;
};

export const isExistByCondition = async (where, repository) => {
  const response = await repository.findOne(where);
  return !!response;
};

export const isExistArrayByCondition = async (where, repository, length) => {
  const response = await repository.findAll(where);
  if (response.length !== length) return false;
  return true;
};

export const errorMessage = {
  idDeleted: 'The resource is deleted',
  idNotDeleted: 'The resource is not deleted',
  name: 'Name is required',
  model: 'Model is required',
  code: 'Code is required',
  serialNumber: 'Serial number is required',
  ratio: 'Ratio is required',
  partnerCode: 'Partner Code is required',
  splitterType: 'Type is required',
  closureType: 'Type is required',
  cableType: 'Type is required',
  colors: 'Type colors must be hexadecimal',
  dimensionItem: 'The item is required',
  inputsItem: 'The item is required',
  tags: 'The tags are required',
  openDirection: 'The open direction is required',
  openDirectionRegex: 'The open direction must be one of TOP/LEFT/RIGHT/BOTTOM',
  number: 'The input must be a number',
  positiveNumber: 'The input must be a positive number',
  json: 'The input must be a JSON',
  integer: 'The input must be an integer',
  array: 'The input must be array',
  address: 'Address is required',
  phone: 'Phone is required',
  phoneRegex: 'The value must be a valid phone number',
  jobName: 'Job name should be min 3 length lowercase letters',
  permissionName: 'Permission name should be min 3 length uppercase letters, numbers or underscore',
  nameTaken: 'The name already exists',
  description: 'Description is required',
  email: 'Please enter a valid email address',
  emailTaken: 'The provided email address is taken',
  value: 'Value is required',
  numericalString: 'Only digits allowed',
  permission: 'Permission is required',
  notExist: 'The specified resource doesn\'t exist',
  key: 'Key is required',
  keyTaken: 'The key is taken',
  date: 'The input value must be dateString or timestamp from 2015 to now',
  offset: 'The offset must be non-negative integer',
  limit: 'The limit must be non-negative integer',
  dateType: 'The dateType must be one of paid/bet',
  active: 'Active is required',
  schedule: 'Schedule is required',
  position: 'The position must be non-negative integer',
  status: 'The status must be integer, from 0 to 5',
  gender: 'The gender must be one of [MALE, FEMALE, NA]',
  orderDirection: 'The order direction must be one of [ASC, DESC]',
  dateFormat: 'The date must be with YYYY-MM-DD format',
  playerId: 'The player id must be positive integer',
  balance: 'The balance must be a positive number',
  id: 'The id must be a positive number',
  geom: 'The geom is required',
  required: 'The field is required',
  queryString: 'The field must be a queryString',
  jsonError: 'The field must be a valid geometry object',
};

export const coreValidator = {
  email: {
    in: ['body'],
    errorMessage: errorMessage.email,
    isEmail: true,
  },
  name: {
    in: ['body'],
    errorMessage: errorMessage.name,
    isEmpty: { negated: true },
  },
  nameInQuery: {
    in: ['query'],
    errorMessage: errorMessage.name,
    isEmpty: { negated: true },
  },
  model: {
    in: ['body'],
    errorMessage: errorMessage.model,
    isEmpty: { negated: true },
  },
  code: {
    in: ['body'],
    errorMessage: errorMessage.code,
    isEmpty: { negated: true },
  },
  ratio: {
    in: ['body'],
    errorMessage: errorMessage.ratio,
    isEmpty: { negated: true },
  },
  offset: {
    in: ['query'],
    isEmpty: { negated: true },
    errorMessage: errorMessage.required,
    isInt: {
      options: { min: 0 },
      errorMessage: errorMessage.offset,
    },
  },
  limit: {
    in: ['query'],
    isEmpty: { negated: true },
    errorMessage: errorMessage.required,
    isInt: {
      options: { min: 0 },
      errorMessage: errorMessage.limit,
    },
  },
  queryString: {
    in: ['query'],
    errorMessage: errorMessage.required,
    matches: {
      options: [/^[a-zA-Z,]{1,}$/],
      errorMessage: errorMessage.queryString,
    },
  },
  partnerCode: {
    in: ['body'],
    errorMessage: errorMessage.partnerCode,
    isEmpty: { negated: true },
  },
  splitterType: {
    in: ['body'],
    errorMessage: errorMessage.splitterType,
    isEmpty: { negated: true },
  },
  dimensionItem: {
    in: ['body'],
    errorMessage: errorMessage.dimensionItem,
    isEmpty: { negated: true },
    isDecimal: {
      errorMessage: errorMessage.number,
    },
  },
  closureType: {
    in: ['body'],
    errorMessage: errorMessage.closureType,
    matches: {
      options: [/\b(?:INDOOR|OUTDOOR)\b/],
      errorMessage: 'The type must be one of INDOOR/OUTDOOR',
    },
  },
  cableType: {
    in: ['body'],
    errorMessage: errorMessage.cableType,
    isEmpty: { negated: true },
  },
  openDirection: {
    in: ['body'],
    errorMessage: errorMessage.openDirection,
    isEmpty: { negated: true },
    matches: {
      options: [/TOP|LEFT|RIGHT|BOTTOM/],
      errorMessage: errorMessage.openDirectionRegex,
    },
  },
  inputs: {
    in: ['body'],
    isArray: { errorMessage: errorMessage.array },
    errorMessage: errorMessage.dimensionItem,
    isEmpty: { negated: true },
    custom: {
      options: (a) => a.every((item) => {
        if (!item.direction || !item.direction.match(/TOP|LEFT|RIGHT|BOTTOM/)) return false;
        if (item.type && !item.type.match(/INPUT|OUTPUT|UNIVERSAL/)) return false;
        if (item.diameter && !Number(item.diameter)) return false;
        return true;
      }),
    },
  },
  colors: {
    in: ['body'],
    isArray: { errorMessage: errorMessage.array },
    errorMessage: errorMessage.colors,
    isEmpty: { negated: true },
    custom: {
      options: (a) => a.every((item) => {
        if (!item.match(/^#[A-Fa-f0-9]{6}$/)) return false;
        return true;
      }),
    },
  },
  tags: {
    in: ['body'],
    isArray: { errorMessage: errorMessage.array },
    errorMessage: errorMessage.tags,
    custom: {
      options: (a) => a.every((item) => {
        if (!item) return false;
        return true;
      }),
    },
  },
  integer: {
    in: ['body'],
    isEmpty: { negated: true },
    isInt: {
      errorMessage: errorMessage.integer,
    },
  },
  number: {
    in: ['body'],
    isEmpty: { negated: true },
    isNumeric: {
      errorMessage: errorMessage.number,
    },
  },
  positiveNumber: {
    in: ['body'],
    isEmpty: { negated: true },
    isFloat: {
      options: { min: 0 },
      errorMessage: errorMessage.positiveNumber,
    },
  },
  json: {
    in: ['body'],
    isEmpty: { negated: true },
    // TODO check if is json
    // isObject: {
    //   errorMessage: errorMessage.json,
    // },
  },
  nameQuery: {
    in: ['query'],
    optional: true,
  },
  address: {
    in: ['body'],
    errorMessage: errorMessage.address,
    isEmpty: { negated: true },
  },
  description: {
    in: ['body'],
    errorMessage: errorMessage.description,
    isEmpty: { negated: true },
  },
  phone: {
    in: ['body'],
    errorMessage: errorMessage.phone,
    isEmpty: { negated: true },
    matches: {
      options: [/^[+]?[0-9]{9,20}$/],
      errorMessage: errorMessage.phoneRegex,
    },
  },
  jobId: {
    in: ['params'],
    errorMessage: (val) => `The resource with ID: ${val} does not exist`,
    custom: { options: (id) => isResourceExist(id, models.job) },
  },
  nodeId: {
    in: ['params'],
    errorMessage: (val) => `The node with ID: ${val} does not exist`,
    custom: { options: (id) => isResourceExist(id, models.node) },
  },
  edgeId: {
    in: ['params'],
    errorMessage: (val) => `The edge with ID: ${val} does not exist`,
    custom: { options: (id) => isResourceExist(id, models.edge) },
  },
  splitterId: {
    in: ['params'],
    errorMessage: (val) => `The splitter with ID: ${val} does not exist`,
    custom: { options: (id) => isResourceExist(id, models.splitter) },
  },
  serverId: {
    in: ['params'],
    errorMessage: (val) => `The server with ID: ${val} does not exist`,
    custom: { options: (id) => isResourceExist(id, models.server) },
  },
  closureId: {
    in: ['params'],
    errorMessage: (val) => `The closure with ID: ${val} does not exist`,
    custom: { options: (id) => isResourceExist(id, models.closure) },
  },
  cableId: {
    in: ['params'],
    errorMessage: (val) => `The cable with ID: ${val} does not exist`,
    custom: { options: (id) => isResourceExist(id, models.cable) },
  },
  brandId: {
    in: ['params'],
    errorMessage: (val) => `The brand with ID: ${val} does not exist`,
    custom: { options: (id) => isResourceExist(id, models.brand) },
  },
  customerId: {
    in: ['params'],
    errorMessage: (val) => `The customer with ID: ${val} does not exist`,
    custom: { options: (id) => isResourceExist(id, models.customer) },
  },
  countryId: {
    in: ['params'],
    errorMessage: (val) => `The country with ID: ${val} does not exist`,
    custom: { options: (id) => isResourceExist(id, models.country) },
  },
  nodeLibId: {
    in: ['params'],
    errorMessage: (val) => `The node lib with ID: ${val} does not exist`,
    custom: { options: (id) => isResourceExist(id, models.nodeLib) },
  },
  fileId: {
    in: ['params'],
    errorMessage: (val) => `The file with ID: ${val} does not exist`,
    custom: { options: (id) => isResourceExist(id, models.file) },
  },
  nodeLibInBody: {
    in: ['body'],
    errorMessage: (val) => `The node lib with ID: ${val} does not exist`,
    custom: { options: (id) => isResourceExist(id, models.nodeLib) },
  },
  serverInBody: {
    in: ['body'],
    errorMessage: (val) => `The server with ID: ${val} does not exist`,
    custom: { options: (id) => isResourceExist(id, models.server) },
  },
  fileInBody: {
    in: ['body'],
    errorMessage: (val) => `The file with ID: ${val} does not exist`,
    custom: { options: (id) => isExistByCondition({ where: { id, type: 'FILE' } }, models.file) },
  },
  fileArrayInBody: {
    in: ['body'],
    isArray: { errorMessage: errorMessage.array },
    errorMessage: (val) => `The files with IDs: ${val} do not exist`,
    custom: { options: (ids) => isExistArrayByCondition({ where: { id: { $in: ids }, type: 'FILE' } }, models.file, ids.length) },
  },
  fileInBodyNullable: {
    in: ['body'],
    errorMessage: (val) => `The file with ID: ${val} does not exist`,
    custom: { options: (id) => (id ? isExistByCondition({ where: { id, type: 'FILE' } }, models.file) : true) },
  },
  folderInBody: {
    in: ['body'],
    errorMessage: (val) => `The folder with ID: ${val} does not exist`,
    custom: { options: (id) => isExistByCondition({ where: { id, type: 'FOLDER' } }, models.file) },
  },
  cableInBody: {
    in: ['body'],
    errorMessage: (val) => `The cable with ID: ${val} does not exist`,
    custom: { options: (id) => isResourceExist(id, models.cable) },
  },
  nodeInBody: {
    in: ['body'],
    errorMessage: (val) => `The node with ID: ${val} does not exist`,
    custom: { options: (id) => isResourceExist(id, models.node) },
  },
  closureInBody: {
    in: ['body'],
    errorMessage: (val) => `The closure with ID: ${val} does not exist`,
    custom: { options: (id) => isResourceExist(id, models.closure) },
  },
  countryInBody: {
    in: ['body'],
    errorMessage: (val) => `The country with ID: ${val} does not exist`,
    custom: { options: (id) => isResourceExist(id, models.country) },
  },
  departmentInBody: {
    in: ['body'],
    errorMessage: (val) => `The department with ID: ${val} does not exist`,
    custom: { options: (id) => isResourceExist(id, models.department) },
  },
  roleInBody: {
    in: ['body'],
    errorMessage: (val) => `The role with ID: ${val} does not exist`,
    custom: { options: (id) => isResourceExist(id, models.role) },
  },
  brandInBody: {
    in: ['body'],
    errorMessage: (val) => `The brand with ID: ${val} does not exist`,
    custom: { options: (id) => isResourceExist(id, models.brand) },
  },
  optionValue: {
    in: ['body'],
    isEmpty: { negated: true },
    isInt: true,
    errorMessage: errorMessage.value,
  },
  active: {
    in: ['body'],
    isEmpty: { negated: true },
    errorMessage: errorMessage.active,
    isBoolean: true,
  },
  schedule: {
    in: ['body'],
    isEmpty: { negated: true },
    isInt: {
      options: { gt: 0 },
      errorMessage: 'The schedule must be positive',
    },
    errorMessage: errorMessage.schedule,
  },
  position: {
    in: ['body'],
    optional: true,
    isInt: {
      options: { min: 0 },
      errorMessage: errorMessage.position,
    },
  },
  from: {
    in: ['query'],
    optional: true,
    custom: {
      optional: true,
      errorMessage: errorMessage.date,
      options: (data) => {
        const dataTime = new Date(data) || new Date(data * 1000);
        return dataTime
          && (dataTime > 1420070400000)
          && (dataTime < Date.now() + 31674256446);
      },
    },
  },
  to: {
    in: ['query'],
    optional: true,
    custom: {
      optional: true,
      errorMessage: errorMessage.date,
      options: (data) => {
        const dataTime = new Date(data) || new Date(data * 1000);
        return dataTime
          && (dataTime > 1420070400000)
          && (dataTime < Date.now() + 31674256446);
      },
    },
  },
  externalId: {
    in: ['body'],
    optional: true,
    isInt: {
      options: { min: 0 },
      errorMessage: errorMessage.id,
    },
  },
};
