/* eslint-disable class-methods-use-this */
import models from '../models';
import { RepositoryError } from '../utils/error_handler';

export default {
  models,

  async startTransaction() {
    const transaction = await models.sequelize.transaction();
    return transaction;
  },

  async commitTransaction(transaction) {
    await transaction.commit();
  },

  async rollBackTransaction(transaction) {
    await transaction.rollBack();
  },

  async findByPk(pk, include, modelName = this.modelName) {
    try {
      const data = await models[modelName].findByPk(pk, {
        ...include,
      });
      return data;
    } catch (error) {
      throw new RepositoryError(error);
    }
  },

  async findOne(where, include, modelName = this.modelName) {
    try {
      const response = await models[modelName].findOne({
        where,
        ...include,
      });
      return response;
    } catch (error) {
      throw new RepositoryError(error);
    }
  },
  async findAll(where, include, modelName = this.modelName) {
    try {
      const response = await models[modelName].findAll({
        where,
        ...include,
      });
      return response;
    } catch (error) {
      throw new RepositoryError(error);
    }
  },
  async create(data, options, modelName = this.modelName) {
    try {
      const response = await models[modelName].create(data, options);
      return response;
    } catch (error) {
      throw new RepositoryError(error);
    }
  },
  async update(data, query, options, modelName = this.modelName) {
    try {
      const response = await models[modelName].update(
        data,
        {
          where: query,
          returning: true,
          plain: true,
          ...options,
        },
      );
      return response[1];
    } catch (error) {
      throw new RepositoryError(error);
    }
  },
  async upsert(data, options, modelName = this.modelName) {
    try {
      const response = await models[modelName].upsert(
        data,
        {
          returning: true,
          plain: true,
          individualHooks: true,
        },
        options,
      );
      return response[0];
    } catch (error) {
      throw new RepositoryError(error);
    }
  },
  async softDelete(query, options, modelName = this.modelName) {
    try {
      const response = await models[modelName].destroy(
        { where: query },
        options,
      );
      return response;
    } catch (error) {
      throw new RepositoryError(error);
    }
  },
  async revert(query, options, modelName = this.modelName) {
    try {
      const response = await models[modelName].update(
        { deletedAt: null },
        { where: query, deletedAt: { ne: null }, paranoid: false },
        options,
      );
      return response;
    } catch (error) {
      throw new RepositoryError(error);
    }
  },
  async destroy(where, modelName = this.modelName) {
    try {
      const response = await models[modelName].destroy({
        where,
        force: true,
      });
      return response;
    } catch (error) {
      throw new RepositoryError(error);
    }
  },
  async bulkUpsert(dataArray, modelName = this.modelName) {
    try {
      const response = await models[modelName].bulkCreate(dataArray, { updateOnDuplicate: ['id'], individualHooks: true });
      return response;
    } catch (error) {
      throw new RepositoryError(error);
    }
  },
};
