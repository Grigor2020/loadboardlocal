/* eslint-disable max-classes-per-file */
/* eslint-disable no-useless-constructor */
export class ErrorHandler extends Error {
  constructor() {
    super();
  }
}
export class BadRequest extends ErrorHandler {
  constructor(
    message = 'The request could not be understood or was missing any required parameters.',
    errors = null,
    name = 'BAD_REQUEST',
    status = 400,
  ) {
    super();
    this.data = null;
    this.message = message;
    this.errors = errors;
    this.status = status;
    this.name = name;
  }
}

export class NotFound extends ErrorHandler {
  constructor(
    message = 'File dose not exist',
    errors = null,
    name = 'NOT_FOUND',
    status = 404,
  ) {
    super();
    this.data = null;
    this.message = message;
    this.errors = errors;
    this.status = status;
    this.name = name;
  }
}

export class AuthError extends ErrorHandler {
  constructor(
    message = 'Authentication failed or user does not have permissions for the requested operation.',
    errors = null,
    name = 'UNAUTHORIZED_ACCESS',
    status = 401,
  ) {
    super();
    this.data = null;
    this.message = message;
    this.errors = errors;
    this.status = status;
    this.name = name;
  }
}

export class ForbiddenError extends ErrorHandler {
  constructor(
    message = 'Access denied and user does not have permissions for the requested operation.',
    errors = null,
    name = 'FORBIDDEN',
    status = 403,
  ) {
    super();
    this.data = null;
    this.message = message;
    this.errors = errors;
    this.status = status;
    this.name = name;
  }
}

export class AccessDenied extends ErrorHandler {
  constructor(
    message = 'Access denied',
    errors = null,
    name = 'FORBIDDEN',
    status = 403,
  ) {
    super();
    this.data = null;
    this.message = message;
    this.errors = errors;
    this.status = status;
    this.name = name;
  }
}

export class ValidationError extends ErrorHandler {
  constructor(
    message = 'The request was well-formed but was unable to be followed due to semantic errors.',
    errors = null,
    name = 'UNPROCESSABLE_ENTITY',
    status = 422,
  ) {
    super();
    this.data = null;
    this.message = message;
    this.errors = errors;
    this.status = status;
    this.name = name;
  }
}

export class RepositoryError extends ErrorHandler {
  constructor(error) {
    super();
    this.data = null;
    this.message = error.message;
    this.errors = error.stack;
    this.status = 500;
    this.name = `REPOSITORY_ERROR: => (${error.name})`;
  }
}

export class ServiceError extends ErrorHandler {
  constructor(error) {
    if (error instanceof ErrorHandler) {
      // eslint-disable-next-line no-param-reassign
      error.name = `SERVICE_ERROR: => ${error.name}`;
      throw error;
    } else {
      super();
      this.data = null;
      this.message = error.message;
      this.errors = error.stack;
      this.status = 500;
      this.name = `SERVICE_ERROR: => (${error.name})`;
    }
  }
}

export class ControllerError extends ErrorHandler {
  constructor(
    message = 'Controller wrapping failed.',
    errors = null,
    name = 'CONTROLLER',
    status = 502,
  ) {
    super();
    this.data = null;
    this.message = message;
    this.errors = errors;
    this.status = status;
    this.name = name;
  }
}
