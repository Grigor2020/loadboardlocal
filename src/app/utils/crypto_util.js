import crypto from 'crypto';

const algorithm = process.env.CRYPTO_ALGORITHM;
const key = process.env.CRYPTO_KEY;
const iv = process.env.CRYPTO_IV;
const salt = process.env.PASSWORD_SALT;

export function createHash(password, userId) {
  return crypto.scryptSync(password, `${userId}${salt}`, 64).toString('hex');
}

export function verify(password, hashPassword, userId) {
  const hashed = createHash(password, userId);
  return hashPassword === hashed;
}

export function encrypt(text) {
  const cipher = crypto.createCipheriv(algorithm, Buffer.from(key), iv);
  let encrypted = cipher.update(text);
  encrypted = Buffer.concat([encrypted, cipher.final()]);
  return encrypted.toString('base64');
}

export function decrypt(text) {
  const decipher = crypto.createDecipheriv(algorithm, Buffer.from(key), iv);
  let decrypted = decipher.update(text);
  decrypted = Buffer.concat([decrypted, decipher.final()]);
  return JSON.parse(decrypted.toString());
}

export function createHashPassword(pasword) {
  return crypto.createHash('sha256').update(pasword).digest('hex');
}
