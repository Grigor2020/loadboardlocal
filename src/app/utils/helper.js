export default function destructQuery(query, include) {
  const options = {
    offset: query.offset || 0,
    limit: query.limit || 20,
    attributes: { },
    include,
  };
  if (query.include) {
    options.attributes = query.include.split(',').filter((field) => options.include.indexOf(field) === -1);
    options.include = options.include.filter((field) => query.include.split(',').indexOf(field) !== -1);
  }
  if (query.exclude) {
    options.include = options.include.filter((field) => query.exclude.split(',').indexOf(field) === -1);
    options.attributes.exclude = query.exclude.split(',');
  }
  return options;
}
