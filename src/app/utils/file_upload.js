import sharp from 'sharp';

import extTypes from '../../../extensionTypes.json';
import { ValidationError } from './error_handler';

const supportedImageExtensionList = ['jpg', 'jpeg', 'png', 'gif', 'bmp', 'svg', 'svg+xml'];
const maxFileLengthInBytes = 2 * 1024 * 1024;

export async function fileUpload(req, res, next) {
  try {
    if (!req.files) {
      return next(new ValidationError('File not found.'));
    }

    const files = Object.values(req.files);
    if (files.length === 0 || !files[0].data) {
      return next(new ValidationError('File not found.'));
    }

    if (files.length > 1 || files[0].length > 0) {
      return next(new ValidationError('A single file is allowed per request.'));
    }

    if (files[0].data.length > maxFileLengthInBytes) {
      return next(new ValidationError('File size exceeds the maximum allowed size.'));
    }

    let format = files[0].mimetype;
    if (!format) {
      return next(new ValidationError('File extension application not found'));
    }
    format = extTypes[format.toLowerCase()];
    if (!format) {
      return next(new ValidationError('File extension is not supported.'));
    }
    if (supportedImageExtensionList.includes(format)) {
      // eslint-disable-next-line no-shadow
      const { format, width, height } = await sharp(files[0].data).metadata();
      req.fileExtension = { format, width, height };
    } else {
      req.fileExtension = { format };
    }
    [req.file] = files;
    return next();
  } catch (error) {
    return next(error);
  }
}

export async function multipleMediaUpload(req, res, next) {
  try {
    if (!req.files || !req.files.media) {
      return next(new ValidationError('No files found.'));
    }
    let mediaArrays = [];
    if (req.files.media && !req.files.media.length) mediaArrays.push(req.files.media);
    else if (req.files.media) mediaArrays = Object.values(req.files.media);

    if (mediaArrays.length > 8) {
      return next(new ValidationError('Maximum 8 media files are allowed per req.'));
    }
    req.extensions = [];
    req.files = [];

    // eslint-disable-next-line no-restricted-syntax
    for await (const file of mediaArrays) {
      if (file.data.length > maxFileLengthInBytes) {
        return next(new ValidationError('File size exceeds the maximum allowed size.'));
      }
      let extension = file.mimetype;
      if (!extension) {
        return next(new ValidationError('File extension application not found'));
      }
      extension = extTypes[extension.toLowerCase()];
      if (!extension) {
        return next(new ValidationError('File extension is not supported.'));
      }
      if (supportedImageExtensionList.includes(extension)) {
        const { format, width, height } = await sharp(file.data).metadata();
        req.extensions.push({ format, width, height });
      } else {
        return next(new ValidationError('File extension is not supported.'));
      }
      req.files.push(file);
    }
    return next();
  } catch (error) {
    return next(error);
  }
}
