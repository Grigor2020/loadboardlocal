/* eslint-disable no-bitwise */
import { ForbiddenError } from './error_handler';

export default async function checkPermissions(req, res, next) {
  try {
    const { userPermission, rolePermission } = req;

    const accessProvided = (userPermission & rolePermission)

    if (accessProvided === 0n) {
      return next(new ForbiddenError('Access denied'));
    }

    return next();
  } catch (error) {
    return next(error);
  }
}
