import crypto from 'crypto';
import url from 'url';
import * as redis from './redis';
import { AuthError } from './error_handler';
import { encrypt } from './crypto_util';
import {authenticateToken} from "./jwt";

const secret = process.env.APP_SECRET;

export default async function authentication(req, res, next) {
  
  try {
    // if (
    //   req.headers.referer === 'https://dev.caiman.am/api-docs'
    //   || req.headers.referer === 'http://localhost:3000/api-docs/') {
    //   // req.user = JSON.parse(USER);
    //   return next();
    // }
    
    // **********************************************
    const authorization = req.headers['authorization'];
    const session = req.headers['x-app-session'];

    console.log('authorization',authorization)
    console.log('session',session)
    // if (!(authorization && session)) {
    //   return next(new AuthError('authorization ad session are required'));
    // }
    // ********************************************************
    
    // const userInfo = await redis.get(token);
    // if (!userInfo) {
    //   return next(new AuthError('Invalid token'));
    // }
    // const { user, sessions } = JSON.parse(userInfo);
    //
    // const userToken = encrypt(user.id);
    //
    // if (userToken !== token) {
    //   return next(new AuthError('Wrong token'));
    // }
    //
    // if (!sessions.includes(session)) {
    //   return next(new AuthError('Invalid session id'));
    // }
    //
    // const uri = url.parse(req.url).pathname.toLowerCase();
    // const method = req.method.toLowerCase();
    // const body = Object.keys(req.body).length ? JSON.stringify(req.body) : '';
    //
    // const signatureRawData = method + uri + body + session + timestamp;
    // const signatureRaw = crypto.createHash('sha256').update(signatureRawData).digest('hex');
    // const hmac = crypto.createHmac('sha256', secret).update(signatureRaw).digest('hex');
    //
    // if (hmac === signature) {
    //   req.user = user;
    //   req.userPermission = BigInt(user.permission || 0);
    //   req.departmentPermission = BigInt(user.department ? user.department.permission : 0);
    //   req.rolePermission = BigInt(user.role ? user.role.permission : 0);
    //   return next();
    // }
    // return next(new AuthError());
    return next();
  } catch (error) {
    return next(error);
  }
}
