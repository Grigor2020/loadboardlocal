export function get(key) {
  return new Promise((resolve, reject) => {
    global.redisClient.get(key, (error, response) => {
      if (error) reject(error);
      resolve(JSON.parse(response));
    });
  });
}

export function del(key) {
  return new Promise((resolve, reject) => {
    global.redisClient.del(key, (error, response) => {
      if (error) reject(error);
      resolve(response);
    });
  });
}

export function keys(key) {
  return new Promise((resolve, reject) => {
    global.redisClient.keys(key, (error, response) => {
      if (error) reject(error);
      resolve(response);
    });
  });
}

export function mGet(key) {
  return new Promise((resolve, reject) => {
    global.redisClient.mget(key, (error, response) => {
      if (error) reject(error);
      resolve(response);
    });
  });
}
