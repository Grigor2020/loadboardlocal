module.exports = (sequelize, DataTypes) => {
  const auth = sequelize.define('auth', {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      unique: true,
    },
    type: {
      type: DataTypes.ENUM,
      values: ['email', 'phone']
    },
    value: {
      type: DataTypes.STRING,
      unique: 'unique_contact'
    },
    verified: {
      type: DataTypes.BOOLEAN,
      unique: 'unique_contact',
      defaultValue: false,
    },
    userId: {
      type: DataTypes.UUID,
      unique: 'unique_contact',
    },
    time: DataTypes.INTEGER.UNSIGNED,
    code: DataTypes.STRING,
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.fn('now'),
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.fn('now'),
    },
  }, {
    tableName: 'auths',
    underscored: true,
    paranoid: true,
    defaultScope: {
      attributes: { exclude: ['deletedAt', 'password'] },
    },
    hooks: {
      afterSave: async (n) => {
        // eslint-disable-next-line no-param-reassign
        if (n && n.dataValues) ['deletedAt', 'password'].forEach((prop) => delete n.dataValues[prop]);
      },
    },
  });
  auth.associate = function associate(models) {
    models.auth.belongsTo(models.user, { foreignKey: 'userId' });
    // models.auth.belongsTo(models.department);
    // models.auth.belongsTo(models.auth);
    // models.auth.belongsTo(models.file, { as: 'profilePicture' });
  };
  return auth;
};
