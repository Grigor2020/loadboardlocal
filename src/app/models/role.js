module.exports = (sequelize, DataTypes) => {
  const role = sequelize.define('role', {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      unique: true,
    },
    name: {
      type: DataTypes.STRING,
      unique: true,
    },
    value: {
      type: DataTypes.STRING
    },
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.fn('now'),
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.fn('now'),
    },
  }, {
    tableName: 'roles',
    underscored: true,
    paranoid: true,
    defaultScope: {
      attributes: { exclude: ['deletedAt', 'password'] },
    },
    hooks: {
      afterSave: async (n) => {
        // eslint-disable-next-line no-param-reassign
        if (n && n.dataValues) ['deletedAt', 'password'].forEach((prop) => delete n.dataValues[prop]);
      },
    },
  });
  role.associate = function associate(models) {
    // models.role.belongsTo(models.country);
    // models.role.belongsTo(models.department);
    // models.role.belongsTo(models.role);
    // models.role.belongsTo(models.file, { as: 'profilePicture' });
  };
  return role;
};
