// const moment = require('moment');
// const axios = require('axios');
const crypto = require('crypto');

// class Caiman {
//   constructor(id, secret, url = 'localhost', port = 3000) {
//     this.id = id;
//     this.secret = secret;
//     this.url = url;
//     this.port = port;
//   }

//   request() {
//     const instance = axios.create({
//       baseURL: `${this.url}:${this.port}`,
//       headers: { 'X-Custom-Header': 'foobar' },
//     });
//   }
// }

module.exports = {
  authentication: (req, res, next) => {
    try {
      return next();
      // const appId = req.headers['x-app-id'];
      // const session = req.headers['x-app-session'];
      // const timestamp = req.headers['x-app-timestamp'];
      // const signature = req.headers['x-app-signature'];

      // // const user = redis.get(appId);
      // const { secret } = JSON.parse(user);
      // const uri = url.parse(req.url).pathname.toLowerCase();
      // const method = req.method.toLowerCase();
      // const body = Object.keys(req.body).length ? JSON.stringify(req.body) : '';

      // const signatureRawData = method + uri + body + session + timestamp;
      // const signatureRaw = crypto
      //   .createHash('sha256')
      //   .update(signatureRawData)
      //   .digest('hex');
      // const hmac = crypto
      //   .createHmac('sha256', secret)
      //   .update(signatureRaw)
      //   .digest('hex');
      // if (hmac === signature) {
      //   req.user = user;
      //   req.permission = user.permission;
      //   return next();
      // }
      // return new AuthError();
    } catch (error) {
      return next(error);
    }
  },

  signRequest: (appSecret, sessionId, uri, method, body) => {
    const headers = {};
    const timeStamp = Date.now() / 1000;
    let reqBody = '';
    if (body) {
      reqBody = JSON.stringify(JSON.parse(reqBody.raw.toString()));
    }

    const signatureRawData = method + uri + reqBody + sessionId + timeStamp;
    const signatureRaw = crypto.SHA256(signatureRawData);
    const signatureRawHex = signatureRaw.toString(crypto.enc.Hex);
    const signatureBytes = crypto.HmacSHA256(signatureRawHex, appSecret);
    const signature = signatureBytes.toString(crypto.enc.Hex);

    // headers.upsert({ key: 'x-app-id', value: AppId });
    headers.upsert({ key: 'x-app-timestamp', value: timeStamp });
    headers.upsert({ key: 'x-app-session', value: sessionId });
    headers.upsert({ key: 'x-app-signature', value: signature });
    // headers.upsert({ key: 'x-app-project', value: projectId });

    return headers;
  },
};
